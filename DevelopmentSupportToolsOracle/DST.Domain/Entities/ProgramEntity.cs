﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DST.Domain.Entities
{
    /// <summary>
    /// プログラムエンティティクラス
    /// </summary>
    [Table("Programs")]
    [Comment("プログラム")]
    public class ProgramEntity : AbstractEntity
    {
        /// <summary>プログラムID</summary>
        [Display(Name = "プログラムID")]
        [Comment("プログラムID")]
        [Required(ErrorMessage = "{0}は必須のため入力してください。")]
        [MaxLength(30, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string ProgramId { get; set; }

        /// <summary>プログラム名</summary>
        [Display(Name = "プログラム名")]
        [Comment("プログラム名")]
        [Required(ErrorMessage = "{0}は必須のため入力してください。")]
        [MaxLength(50, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Name { get; set; }

        /// <summary>目的</summary>
        [Display(Name = "目的", Description = "このプログラムで何を実現したいか。NG:発注状況を照会するため。OK:発注状況を照会し〇〇をするため。")]
        [Comment("目的")]
        [MaxLength(500, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Purpose { get; set; }

        /// <summary>機能概要</summary>
        [Display(Name = "機能概要")]
        [Comment("機能概要")]
        [MaxLength(500, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string FunctionalOverview { get; set; }

        /// <summary>サブシステムID</summary>
        [Display(Name = "サブシステムID")]
        [Comment("サブシステムシステムID")]
        public int SubSystemId { get; set; }

        /// <summary>サブシステム情報</summary>
        [Display(Name = "サブシステム情報")]
        [ForeignKey(nameof(SubSystemId))]
        [InverseProperty(nameof(SubSystemEntity.Programs))]
        public virtual SubSystemEntity SubSystem { get; set; }
    }
}