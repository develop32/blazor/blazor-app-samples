﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DST.Domain.Entities
{
    /// <summary>
    /// システムエンティティクラス
    /// </summary>
    [Table("Systems")]
    [Comment("システム")]
    public class SystemEntity : AbstractEntity
    {
        /// <summary>システム区分</summary>
        [Display(Name = "システム区分")]
        [Comment("システム区分:Iは入力不可。")]
        [Required(ErrorMessage = "{0}は必須のため入力してください。")]
        [RegularExpression(@"[A-HJ-Z]", ErrorMessage = "半角英字(A-Z)1文字を入力してください。Iは入力不可。")]
        [MaxLength(1, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Compartment { get; set; }

        /// <summary>システム名</summary>
        [Display(Name = "システム名")]
        [Comment("システム名")]
        [Required(ErrorMessage = "{0}は必須のため入力してください。")]
        [MaxLength(50, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Name { get; set; }

        /// <summary>システム区分名称</summary>
        [Display(Name = "システム区分名称")]
        public string CompartmentName
        {
            get
            {
                return $"{Compartment} {Name}";
            }
        }

        /// <summary>説明</summary>
        [Display(Name = "説明")]
        [Comment("説明")]
        [MaxLength(300, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Description { get; set; }

        /// <summary>サブシステム情報</summary>
        [Display(Name = "サブシステム情報")]
        [InverseProperty(nameof(SubSystemEntity.System))]
        public virtual ICollection<SubSystemEntity> SubSystems { get; set; }
    }
}