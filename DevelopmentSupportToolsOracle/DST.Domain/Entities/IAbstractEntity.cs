﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DST.Domain.Entities
{
    /// <summary>
    /// 規定エンティティインターフェース
    /// </summary>
    public interface IAbstractEntity
    {
        /// <summary>ID</summary>
        [Display(Name = "ID")]
        public int Id { get; set; }

        /// <summary>作成者</summary>
        [Display(Name = "作成者")]
        public string CreatedBy { get; set; }

        /// <summary>作成日時</summary>
        [Display(Name = "作成日時")]
        public DateTime CreatedOn { get; set; }

        /// <summary>更新者</summary>
        [Display(Name = "更新者")]
        public string UpdatedBy { get; set; }

        /// <summary>更新日時</summary>
        [Display(Name = "更新日時")]
        public DateTime? UpdatedOn { get; set; }
    }
}