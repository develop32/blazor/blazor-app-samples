﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DST.Domain.Entities
{
    /// <summary>
    /// サブシステムエンティティクラス
    /// </summary>
    [Table("SubSystems")]
    [Comment("サブシステム")]
    public class SubSystemEntity : AbstractEntity
    {
        /// <summary>サブシステム区分</summary>
        [Display(Name = "サブシステム区分")]
        [Comment("サブシステム区分:Iは入力不可。")]
        [Required(ErrorMessage = "{0}は必須のため入力してください。")]
        [RegularExpression(@"[A-HJ-Z]", ErrorMessage = "半角英字(A-Z)1文字を入力してください。Iは入力不可。")]
        [MaxLength(1, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Compartment { get; set; }

        /// <summary>サブシステム区分名称</summary>
        [Display(Name = "サブシステム区分名称")]
        public string CompartmentName
        {
            get
            {
                return $"{Compartment} {Name}";
            }
        }

        /// <summary>システム名</summary>
        [Display(Name = "サブシステム名")]
        [Comment("サブシステム名")]
        [Required(ErrorMessage = "{0}は必須のため入力してください。")]
        [MaxLength(50, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Name { get; set; }

        /// <summary>説明</summary>
        [Display(Name = "説明")]
        [Comment("説明")]
        [MaxLength(300, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Description { get; set; }

        /// <summary>システムID（FK）</summary>
        [Display(Name = "システムID（FK）")]
        [Comment("システムID")]
        public int SystemId { get; set; }

        /// <summary>システム情報</summary>
        [Display(Name = "システム情報")]
        [ForeignKey(nameof(SystemId))]
        [InverseProperty(nameof(SystemEntity.SubSystems))]
        public virtual SystemEntity System { get; set; }

        /// <summary>プログラム情報</summary>
        [Display(Name = "プログラム情報")]
        [InverseProperty(nameof(ProgramEntity.SubSystem))]
        public virtual ICollection<ProgramEntity> Programs { get; set; }
    }
}