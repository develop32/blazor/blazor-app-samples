﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace DST.Domain.Entities
{
    /// <summary>
    /// 基底エンティティクラス
    /// </summary>
    public abstract class AbstractEntity : IAbstractEntity
    {
        /// <inheritdoc/>
        [Comment("ID")]
        [Key]
        public int Id { get; set; }

        /// <inheritdoc/>
        [Comment("作成者")]
        [Required(ErrorMessage = "{0}は必須のため入力してください。")]
        [MaxLength(50)]
        public string CreatedBy { get; set; }

        /// <inheritdoc/>
        [Comment("作成日時")]
        [Required(ErrorMessage = "{0}は必須のため入力してください。")]
        public DateTime CreatedOn { get; set; }

        /// <inheritdoc/>
        [Comment("更新者")]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }

        /// <inheritdoc/>
        [Comment("更新日時")]
        public DateTime? UpdatedOn { get; set; }
    }
}