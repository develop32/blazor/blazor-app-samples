﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DST.Domain.Entities;

namespace DST.Domain.Repositories
{
    /// /// <summary>
    /// システムレポジトリインターフェース
    /// </summary>
    public interface ISystemsRepository
    {
        /// <summary>
        /// すべてのシステム情報を取得します。
        /// </summary>
        /// <returns>すべてのシステム情報</returns>
        public Task<IEnumerable<SystemEntity>> GetAllSystems();
    }
}