﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DST.Domain.Entities;

namespace DST.Domain.Repositories
{
    /// <summary>
    /// サブシステムレポジトリインターフェース
    /// </summary>
    public interface ISubSystemsRepository
    {
        /// <summary>
        /// システムIDよりサブシステム一覧を取得します。
        /// </summary>
        /// <param name="systemId">システムID</param>
        /// <returns>サブシステム一覧</returns>
        public Task<IEnumerable<SubSystemEntity>> GetSubSystemsBySystemId(int systemId);
    }
}