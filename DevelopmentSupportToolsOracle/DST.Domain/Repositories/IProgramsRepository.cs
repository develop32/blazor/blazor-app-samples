﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DST.Domain.Dtos;
using DST.Domain.Entities;

namespace DST.Domain.Repositories
{
    /// <summary>
    /// プログラムレポジトリインターフェース
    /// </summary>
    public interface IProgramsRepository
    {
        /// <summary>
        /// プログラムを登録します。
        /// </summary>
        /// <param name="program">プログラム</param>
        /// <returns>追加されたプログラム</returns>
        public Task<ProgramEntity> CreateProgram(ProgramEntity program);

        /// <summary>
        /// プログラムを削除します。
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns>削除件数</returns>
        public Task<int> DeleteProgram(int id);

        /// <summary>
        /// すべてのプログラムを取得します。
        /// </summary>
        /// <returns>すべてプログラム</returns>
        public Task<IEnumerable<ProgramEntity>> GetPrograms(ProgramListSearchItemDto seachItem);

        /// <summary>
        /// プログラムを取得します。
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns>プログラム</returns>
        public Task<ProgramEntity> GetProgram(int id);

        /// <summary>
        /// プログラムを更新します。
        /// </summary>
        /// <param name="id">ID</param>
        /// <param name="program">更新するプログラム</param>
        /// <returns>更新されたプログラム</returns>
        public Task<ProgramEntity> UpdateProgram(int id, ProgramEntity program);
    }
}