﻿using System.ComponentModel.DataAnnotations;

namespace DST.Domain.Dtos
{
    /// <summary>
    /// プログラム一覧検索項目DTOクラス
    /// </summary>
    public class ProgramListSearchItemDto
    {
        /// <summary>システムID</summary>
        [Display(Name = "システムID")]
        public int SystemId { get; set; }

        /// <summary>サブシステムID</summary>
        [Display(Name = "サブシステムID")]
        public int SubSystemId { get; set; }

        /// <summary>検索文字列</summary>
        [Display(Name = "検索文字列")]
        public string SearchString { get; set; }
    }
}