﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DST.Infrastructure.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Systems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "ID")
                        .Annotation("Oracle:Identity", "START WITH 1 INCREMENT BY 1"),
                    Compartment = table.Column<string>(type: "NVARCHAR2(1)", maxLength: 1, nullable: false, comment: "システム区分:Iは入力不可。"),
                    Name = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: false, comment: "システム名"),
                    Description = table.Column<string>(type: "NVARCHAR2(300)", maxLength: 300, nullable: true, comment: "説明"),
                    CreatedBy = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: false, comment: "作成者"),
                    CreatedOn = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false, comment: "作成日時"),
                    UpdatedBy = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true, comment: "更新者"),
                    UpdatedOn = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true, comment: "更新日時")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Systems", x => x.Id);
                },
                comment: "システム");

            migrationBuilder.CreateTable(
                name: "SubSystems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "ID")
                        .Annotation("Oracle:Identity", "START WITH 1 INCREMENT BY 1"),
                    Compartment = table.Column<string>(type: "NVARCHAR2(1)", maxLength: 1, nullable: false, comment: "サブシステム区分:Iは入力不可。"),
                    Name = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: false, comment: "サブシステム名"),
                    Description = table.Column<string>(type: "NVARCHAR2(300)", maxLength: 300, nullable: true, comment: "説明"),
                    SystemId = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "システムID"),
                    CreatedBy = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: false, comment: "作成者"),
                    CreatedOn = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false, comment: "作成日時"),
                    UpdatedBy = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true, comment: "更新者"),
                    UpdatedOn = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true, comment: "更新日時")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubSystems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubSystems_Systems_SystemId",
                        column: x => x.SystemId,
                        principalTable: "Systems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "サブシステム");

            migrationBuilder.CreateTable(
                name: "Programs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "ID")
                        .Annotation("Oracle:Identity", "START WITH 1 INCREMENT BY 1"),
                    ProgramId = table.Column<string>(type: "NVARCHAR2(30)", maxLength: 30, nullable: false, comment: "プログラムID"),
                    Name = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: false, comment: "プログラム名"),
                    Purpose = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true, comment: "目的"),
                    FunctionalOverview = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true, comment: "機能概要"),
                    SubSystemId = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "サブシステムシステムID"),
                    CreatedBy = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: false, comment: "作成者"),
                    CreatedOn = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false, comment: "作成日時"),
                    UpdatedBy = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true, comment: "更新者"),
                    UpdatedOn = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true, comment: "更新日時")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Programs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Programs_SubSystems_SubSystemId",
                        column: x => x.SubSystemId,
                        principalTable: "SubSystems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "プログラム");

            migrationBuilder.InsertData(
                table: "Systems",
                columns: new[] { "Id", "Compartment", "CreatedBy", "CreatedOn", "Description", "Name", "UpdatedBy", "UpdatedOn" },
                values: new object[] { 1, "A", "Administrator", new DateTime(2021, 11, 23, 12, 28, 55, 89, DateTimeKind.Local).AddTicks(9490), "主に営業担当者が利用するシステム。", "営業", null, null });

            migrationBuilder.InsertData(
                table: "Systems",
                columns: new[] { "Id", "Compartment", "CreatedBy", "CreatedOn", "Description", "Name", "UpdatedBy", "UpdatedOn" },
                values: new object[] { 2, "B", "Administrator", new DateTime(2021, 11, 23, 12, 28, 55, 91, DateTimeKind.Local).AddTicks(221), "主に設計担当者が利用するシステム。", "設計", null, null });

            migrationBuilder.InsertData(
                table: "SubSystems",
                columns: new[] { "Id", "Compartment", "CreatedBy", "CreatedOn", "Description", "Name", "SystemId", "UpdatedBy", "UpdatedOn" },
                values: new object[] { 1, "A", "Administrator", new DateTime(2021, 11, 23, 12, 28, 55, 92, DateTimeKind.Local).AddTicks(5280), "受注に関するするシステム。", "受注管理", 1, null, null });

            migrationBuilder.InsertData(
                table: "SubSystems",
                columns: new[] { "Id", "Compartment", "CreatedBy", "CreatedOn", "Description", "Name", "SystemId", "UpdatedBy", "UpdatedOn" },
                values: new object[] { 2, "B", "Administrator", new DateTime(2021, 11, 23, 12, 28, 55, 92, DateTimeKind.Local).AddTicks(5300), "出荷に関するするシステム。", "出荷管理", 1, null, null });

            migrationBuilder.InsertData(
                table: "SubSystems",
                columns: new[] { "Id", "Compartment", "CreatedBy", "CreatedOn", "Description", "Name", "SystemId", "UpdatedBy", "UpdatedOn" },
                values: new object[] { 3, "A", "Administrator", new DateTime(2021, 11, 23, 12, 28, 55, 92, DateTimeKind.Local).AddTicks(5302), "設計に関するするシステム。", "設計管理", 2, null, null });

            migrationBuilder.CreateIndex(
                name: "IX_Programs_SubSystemId",
                table: "Programs",
                column: "SubSystemId");

            migrationBuilder.CreateIndex(
                name: "IX_SubSystems_SystemId",
                table: "SubSystems",
                column: "SystemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Programs");

            migrationBuilder.DropTable(
                name: "SubSystems");

            migrationBuilder.DropTable(
                name: "Systems");
        }
    }
}
