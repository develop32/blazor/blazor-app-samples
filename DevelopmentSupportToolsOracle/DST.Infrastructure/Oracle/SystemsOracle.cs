﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DST.Domain.Entities;
using DST.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DST.Infrastructure.Oracle
{
    /// <summary>
    /// システムレポジトリインターフェース
    /// </summary>
    public class SystemsOracle : ISystemsRepository
    {
        private readonly ApplicationDbContext _dbContext;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        public SystemsOracle(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<SystemEntity>> GetAllSystems()
        {
            return await _dbContext.Systems.ToListAsync();
        }
    }
}