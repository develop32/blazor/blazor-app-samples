﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DST.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DST.Infrastructure.Oracle
{
    /// <summary>
    /// アプリケーションDBコンテキスト
    /// </summary>
    public class ApplicationDbContext : DbContext
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="options">オプション</param>
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }

        /// <summary>システムテーブル</summary>
        public virtual DbSet<SystemEntity> Systems { get; set; }

        /// <summary>サブシステムテーブル</summary>
        public virtual DbSet<SubSystemEntity> SubSystems { get; set; }

        /// <summary>プログラムテーブル</summary>
        public virtual DbSet<ProgramEntity> Programs { get; set; }

        /// <inheritdoc/>
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
        {
            foreach (var entry in ChangeTracker.Entries<IAbstractEntity>().ToList())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedOn = DateTime.Now;
                        entry.Entity.CreatedBy = "Unknown";
                        entry.Entity.UpdatedOn = DateTime.Now;
                        entry.Entity.UpdatedBy = "Unknown";
                        break;

                    case EntityState.Modified:
                        entry.Entity.UpdatedOn = DateTime.Now;
                        entry.Entity.UpdatedBy = "Unknown";
                        break;
                }
            }

            return await base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SystemEntity>().HasData(
                new SystemEntity
                {
                    Id = 1,
                    Compartment = "A",
                    Name = "営業",
                    Description = "主に営業担当者が利用するシステム。",
                    CreatedBy = "Administrator",
                    CreatedOn = DateTime.Now,
                },
                new SystemEntity
                {
                    Id = 2,
                    Compartment = "B",
                    Name = "設計",
                    Description = "主に設計担当者が利用するシステム。",
                    CreatedBy = "Administrator",
                    CreatedOn = DateTime.Now,
                });
            modelBuilder.Entity<SubSystemEntity>().HasData(
                new SubSystemEntity
                {
                    Id = 1,
                    Compartment = "A",
                    Name = "受注管理",
                    Description = "受注に関するするシステム。",
                    SystemId = 1,
                    CreatedBy = "Administrator",
                    CreatedOn = DateTime.Now,
                },
                new SubSystemEntity
                {
                    Id = 2,
                    Compartment = "B",
                    Name = "出荷管理",
                    Description = "出荷に関するするシステム。",
                    SystemId = 1,
                    CreatedBy = "Administrator",
                    CreatedOn = DateTime.Now,
                },
                new SubSystemEntity
                {
                    Id = 3,
                    Compartment = "A",
                    Name = "設計管理",
                    Description = "設計に関するするシステム。",
                    SystemId = 2,
                    CreatedBy = "Administrator",
                    CreatedOn = DateTime.Now,
                });
        }
    }
}