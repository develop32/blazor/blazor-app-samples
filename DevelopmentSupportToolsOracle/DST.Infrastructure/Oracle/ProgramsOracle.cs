﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DST.Domain.Dtos;
using DST.Domain.Entities;
using DST.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DST.Infrastructure.Oracle
{
    /// <summary>
    /// プログラムテーブルアクセスクラス
    /// </summary>
    public class ProgramsOracle : IProgramsRepository
    {
        private readonly ApplicationDbContext _dbContext;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        public ProgramsOracle(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <inheritdoc/>
        public async Task<ProgramEntity> CreateProgram(ProgramEntity program)
        {
            var addedProgram = await _dbContext.Programs.AddAsync(program);
            await _dbContext.SaveChangesAsync();
            return addedProgram.Entity;
        }

        /// <inheritdoc/>
        public Task<int> DeleteProgram(int id)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<ProgramEntity>> GetPrograms(ProgramListSearchItemDto seachItem)
        {
            return await _dbContext.Programs
                .Include(p => p.SubSystem.System)
                .Where(p => seachItem.SystemId == 0 || p.SubSystem.System.Id == seachItem.SystemId)
                .Where(p => seachItem.SubSystemId == 0 || p.SubSystem.Id == seachItem.SubSystemId)
                .Where(p => string.IsNullOrEmpty(seachItem.SearchString) ||
                       p.ProgramId.Contains(seachItem.SearchString) ||
                       p.Name.Contains(seachItem.SearchString) ||
                       p.Purpose.Contains(seachItem.SearchString) ||
                       p.FunctionalOverview.Contains(seachItem.SearchString))
                .OrderBy(p => p.SubSystem.System.Compartment)
                .ThenBy(p => p.SubSystem.Compartment)
                .ToListAsync();
        }

        /// <inheritdoc/>
        public async Task<ProgramEntity> GetProgram(int id)
        {
            return await _dbContext.Programs
                .Include(p => p.SubSystem)
                .FirstOrDefaultAsync(p => p.Id == id);
        }

        /// <inheritdoc/>
        public Task<ProgramEntity> UpdateProgram(int id, ProgramEntity program)
        {
            throw new System.NotImplementedException();
        }
    }
}