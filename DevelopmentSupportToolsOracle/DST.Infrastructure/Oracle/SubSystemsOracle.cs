﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DST.Domain.Entities;
using DST.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DST.Infrastructure.Oracle
{
    /// <summary>
    /// サブシステムテーブルアクセスクラス
    /// </summary>
    public class SubSystemsOracle : ISubSystemsRepository
    {
        private readonly ApplicationDbContext _dbContext;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        public SubSystemsOracle(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<SubSystemEntity>> GetSubSystemsBySystemId(int systemId)
        {
            return await _dbContext.SubSystems.Where(s => s.SystemId == systemId).ToListAsync();
        }
    }
}