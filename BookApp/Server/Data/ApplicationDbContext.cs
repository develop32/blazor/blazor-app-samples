﻿using BookApp.Shared.Entities;
using Microsoft.EntityFrameworkCore;

namespace BookApp.Server.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Book> Books { get; set; }
    }
}
