﻿using System.ComponentModel.DataAnnotations;

namespace BookApp.Shared.Entities
{
    public class Book
    {
        public int BookId { get; set; }
        [Required]
        public string Title { get; set; }
        public string Author { get; set; }
    }
}
