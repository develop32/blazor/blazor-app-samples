﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DST.Shared.Entities
{
    /// <summary>
    /// プログラムエンティティクラス。
    /// </summary>
    [Table("Programs")]
    public class ProgramEntity : AbstractEntity<int>
    {
        /// <summary>プログラム名</summary>
        [Display(Name = "プログラムID")]
        [Required]
        [MaxLength(50, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string ProgramId { get; set; }

        /// <summary>プログラム名</summary>
        [Display(Name = "プログラム名")]
        [Required]
        [MaxLength(50, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string ProgramName { get; set; }

        /// <summary>目的</summary>
        [Display(Name = "目的", Description = "このプログラムで何を実現したいか。NG:発注状況を照会するため。OK:発注状況を照会し〇〇をするため。")]
        [MaxLength(500, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Purpose { get; set; }

        /// <summary>機能概要</summary>
        [Display(Name = "機能概要")]
        [MaxLength(500, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string FunctionalOverview { get; set; }

        /// <summary>サブシステム区分ID</summary>
        [Display(Name = "サブシステム区分ID")]
        public int SubSystemCompartmentId { get; set; }

        /// <summary>サブシステム区分情報</summary>
        [Display(Name = "サブシステム区分情報")]
        [ForeignKey(nameof(SubSystemCompartmentId))]
        public virtual SubSystemCompartmentEntity SubSystemCompartment { get; set; }

        /// <summary>タグ情報</summary>
        [Display(Name = "タグ情報")]
        public virtual ICollection<TagEntity> Tags { get; set; }
    }
}