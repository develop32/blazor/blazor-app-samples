﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DST.Shared.Entities
{
    /// <summary>
    /// システム区分エンティティクラス。
    /// </summary>
    [Table("SystemCompartments")]
    [Index(nameof(Compartment), IsUnique = true)]
    public class SystemCompartmentEntity : AbstractEntity<int>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public SystemCompartmentEntity()
        {
            SubSystemCompartments = new HashSet<SubSystemCompartmentEntity>();
        }

        /// <summary>システム区分</summary>
        [Display(Name = "システム区分", Description = "半角英字(A-Z)1文字を入力してください。")]
        [Required(ErrorMessage = "{0}は必須のため入力してください。")]
        [RegularExpression(@"[A-HJ-Z]", ErrorMessage = "半角英字(A-Z)1文字を入力してください。Iは入力不可。")]
        [MaxLength(1, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Compartment { get; set; }

        /// <summary>システム区分名</summary>
        [Display(Name = "システム区分名")]
        [Required(ErrorMessage = "{0}は必須のため入力してください。")]
        public string CompartmentName { get; set; }

        /// <summary>説明</summary>
        [Display(Name = "説明")]
        [MaxLength(300, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Description { get; set; }

        /// <summary>備考</summary>
        [Display(Name = "備考")]
        [MaxLength(500, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Remarks { get; set; }

        /// <summary>サブシステム区分情報</summary>
        [Display(Name = "サブシステム区分情報")]
        [InverseProperty(nameof(SubSystemCompartmentEntity.SystemCompartment))]
        public virtual ICollection<SubSystemCompartmentEntity> SubSystemCompartments { get; set; }

        public void Check()
        {
            if (Compartment == "I")
            {
                throw new ApplicationException("I");
            }
        }
    }
}