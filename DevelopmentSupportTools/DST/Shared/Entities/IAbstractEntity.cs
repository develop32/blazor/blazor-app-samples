﻿using System;

namespace DST.Shared.Entities
{
    public interface IAbstractEntity
    {
        /// <summary>作成者</summary>
        public string CreatedBy { get; set; }

        /// <summary>作成日時</summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>更新者</summary>
        public string UpdatedBy { get; set; }

        /// <summary>更新日時</summary>
        public DateTime UpdatedOn { get; set; }
    }
}