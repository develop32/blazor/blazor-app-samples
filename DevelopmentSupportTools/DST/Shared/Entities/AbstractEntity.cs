﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DST.Shared.Entities
{
    /// <summary>
    /// 基底エンティティクラス。
    /// </summary>
    /// <typeparam name="TId">Idの型</typeparam>
    public abstract class AbstractEntity<TId> : IAbstractEntity
    {
        /// <summary>ID</summary>
        [Display(Name = "ID")]
        public TId Id { get; set; }

        /// <inheritdoc/>
        [Display(Name = "作成者")]
        [Required]
        [MaxLength(50)]
        public string CreatedBy { get; set; } = "Unknown";

        /// <inheritdoc/>
        [Display(Name = "作成日時")]
        [Required]
        public DateTime CreatedOn { get; set; } = DateTime.Now;

        /// <inheritdoc/>
        [Display(Name = "更新者")]
        [Required]
        [MaxLength(50)]
        public string UpdatedBy { get; set; } = "Unknown";

        /// <inheritdoc/>
        [Display(Name = "更新日時")]
        [Required]
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
    }
}