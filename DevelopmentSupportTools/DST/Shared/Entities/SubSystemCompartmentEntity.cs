﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DST.Shared.Entities
{
    /// <summary>
    /// サブシステム区分エンティティクラス。
    /// </summary>
    [Table("SubSystemCompartments")]
    [Index(nameof(Compartment), IsUnique = true)]
    public class SubSystemCompartmentEntity : AbstractEntity<int>
    {
        /// <summary>サブシステム区分</summary>
        [Display(Name = "サブシステム区分")]
        [Required(ErrorMessage = "{0}は必須のため入力してください。")]
        [RegularExpression(@"[A-Z]", ErrorMessage = "半角英字(A-Z)のみ入力できます")]
        [MaxLength(1, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Compartment { get; set; }

        /// <summary>サブシステム区分名</summary>
        [Display(Name = "サブシステム区分名")]
        [Required(ErrorMessage = "{0}は必須のため入力してください。")]
        public string CompartmentName { get; set; }

        /// <summary>システムサブシステム区分</summary>
        [Display(Name = "システムサブシステム区分")]
        public string CompartmentWithSystem
        {
            get
            {
                return $"{SystemCompartment?.Compartment}-{Compartment}";
            }
        }

        /// <summary>システムサブシステム区分名</summary>
        [Display(Name = "システムサブシステム区分名")]
        public string CompartmentNameWithSystem
        {
            get
            {
                return $"{SystemCompartment?.CompartmentName}-{CompartmentName}";
            }
        }

        /// <summary>説明</summary>
        [Display(Name = "説明")]
        [MaxLength(300, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Description { get; set; }

        /// <summary>備考</summary>
        [Display(Name = "備考")]
        [MaxLength(500, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Remarks { get; set; }

        /// <summary>システム区分ID</summary>
        [Display(Name = "システム区分ID")]
        public int SystemCompartmentId { get; set; }

        /// <summary>システム区分情報</summary>
        [Display(Name = "システム区分情報")]
        [ForeignKey(nameof(SystemCompartmentId))]
        [InverseProperty(nameof(SystemCompartmentEntity.SubSystemCompartments))]
        public virtual SystemCompartmentEntity SystemCompartment { get; set; }
    }
}