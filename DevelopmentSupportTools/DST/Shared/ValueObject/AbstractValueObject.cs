﻿namespace DST.Shared.ValueObjects
{
    /// <summary>
    /// 値オブジェクトの基底クラス
    /// </summary>
    /// <typeparam name="T">タイプ</typeparam>
    public abstract class AbstractValueObject<T>
        where T : AbstractValueObject<T>
    {
        /// <summary>
        /// ==
        /// </summary>
        /// <param name="vo1">vo1</param>
        /// <param name="vo2">vo2</param>
        /// <returns>Trueの場合、等しい</returns>
        public static bool operator ==(AbstractValueObject<T> vo1, AbstractValueObject<T> vo2)
        {
            return Equals(vo1, vo2);
        }

        /// <summary>
        /// !=
        /// </summary>
        /// <param name="vo1">vo1</param>
        /// <param name="vo2">vo2</param>
        /// <returns>Trueの場合、等しくない</returns>
        public static bool operator !=(AbstractValueObject<T> vo1, AbstractValueObject<T> vo2)
        {
            return !Equals(vo1, vo2);
        }

        /// <summary>
        /// Equals
        /// </summary>
        /// <param name="obj">obj</param>
        /// <returns>Trueの場合、等しい</returns>
        public override bool Equals(object obj)
        {
            var vo = obj as T;
            if (vo == null)
            {
                return false;
            }

            return EqualsCore(vo);
        }

        /// <summary>
        /// ToString
        /// </summary>
        /// <returns>文字列</returns>
        public override string ToString()
        {
            return base.ToString();
        }

        /// <summary>
        /// GetHashCode
        /// </summary>
        /// <returns>値</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// EqualsCore
        /// </summary>
        /// <param name="other">other</param>
        /// <returns>Trueの場合、等しい</returns>
        protected abstract bool EqualsCore(T other);
    }
}