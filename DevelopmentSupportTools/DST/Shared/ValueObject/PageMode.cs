﻿using System;

namespace DST.Shared.ValueObjects
{
    /// <summary>
    /// ページモード。
    /// </summary>
    public class PageMode : AbstractValueObject<PageMode>
    {
        /// <summary>新規作成</summary>
        public static readonly PageMode Create = new(1);

        /// <summary>参照</summary>
        public static readonly PageMode Show = new(2);

        /// <summary>編集</summary>
        public static readonly PageMode Edit = new(3);

        /// <summary>削除</summary>
        public static readonly PageMode Delete = new(4);

        /// <summary>
        /// コンストラクタ。
        /// </summary>
        /// <param name="value">値</param>
        public PageMode(int value)
        {
            Value = value;
        }

        /// <summary>
        /// コンストラクタ。
        /// </summary>
        /// <param name="name">名前</param>
        public PageMode(string name)
        {
            if (name == nameof(Create))
            {
                Value = Create.Value;
                return;
            }

            if (name == nameof(Show))
            {
                Value = Show.Value;
                return;
            }

            if (name == nameof(Edit))
            {
                Value = Edit.Value;
                return;
            }

            if (name == nameof(Delete))
            {
                Value = Delete.Value;
                return;
            }

            throw new ApplicationException($"不正なページモードです。(name={name})");
        }

        /// <summary>値</summary>
        public int Value { get; }

        /// <summary>表示値</summary>
        public string DisplayValue
        {
            get
            {
                if (this == Create)
                {
                    return "新規作成";
                }

                if (this == Show)
                {
                    return "参照";
                }

                if (this == Edit)
                {
                    return "編集";
                }

                if (this == Delete)
                {
                    return "削除";
                }

                throw new ApplicationException($"不正なページモードです。(Value={Value})");
            }
        }

        /// <summary>
        /// Trueの場合、読み取り専用です。
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                if (Value == Create.Value || Value == Edit.Value)
                {
                    return false;
                }

                if (Value == Show.Value || Value == Delete.Value)
                {
                    return true;
                }

                throw new ApplicationException($"不正なページモードです。(Value={Value})");
            }
        }

        /// <summary>
        /// 他のオブジェクトと比較する
        /// </summary>
        /// <param name="other">他のオブジェクト</param>
        /// <returns>Trueの場合、等しい。</returns>
        protected override bool EqualsCore(PageMode other)
        {
            return Value == other.Value;
        }
    }
}