﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DST.Server.Migrations
{
    public partial class AddRemarksToSubSystemCompartments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Remarks",
                table: "SubSystemCompartments",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Remarks",
                table: "SubSystemCompartments");
        }
    }
}
