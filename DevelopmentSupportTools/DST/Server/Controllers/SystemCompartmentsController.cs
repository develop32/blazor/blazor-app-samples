﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DST.Server.Data;
using DST.Shared.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DST.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SystemCompartmentsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public SystemCompartmentsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/SystemCompartments
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SystemCompartmentEntity>>> GetSystemCompartments()
        {
            return await _context.SystemCompartments
                .ToListAsync();
        }

        // GET: api/SystemCompartments/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SystemCompartmentEntity>> GetSystemCompartment(int id)
        {
            var systemCompartment = await _context.SystemCompartments.FindAsync(id);

            if (systemCompartment == null)
            {
                return NotFound();
            }

            return systemCompartment;
        }

        // PUT: api/SystemCompartments/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSystemCompartment(int id, SystemCompartmentEntity systemCompartment)
        {
            if (id != systemCompartment.Id)
            {
                return BadRequest();
            }

            _context.Entry(systemCompartment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExistsSystemCompartment(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SystemCompartments
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<SystemCompartmentEntity>> PostSystemCompartment(SystemCompartmentEntity systemCompartment)
        {
            if (_context.SystemCompartments.Where(s => s.Compartment == systemCompartment.Compartment).Any())
            {
            }
            _context.SystemCompartments.Add(systemCompartment);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSystemCompartment", new { id = systemCompartment.Id }, systemCompartment);
        }

        // DELETE: api/SystemCompartments/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSystemCompartment(int id)
        {
            var systemCompartment = await _context.SystemCompartments.FindAsync(id);
            if (systemCompartment == null)
            {
                return NotFound();
            }

            _context.SystemCompartments.Remove(systemCompartment);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ExistsSystemCompartment(int id)
        {
            return _context.SystemCompartments.Any(e => e.Id == id);
        }
    }
}