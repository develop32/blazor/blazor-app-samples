﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DST.Server.Data;
using DST.Shared.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DST.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProgramsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ProgramsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Programs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProgramEntity>>> GetPrograms()
        {
            return await _context.Programs
                .Include(p => p.SubSystemCompartment.SystemCompartment)
                .OrderBy(p => p.SubSystemCompartment.SystemCompartment.Compartment)
                .ThenBy(p => p.SubSystemCompartment.Compartment)
                .ToListAsync();
        }

        // GET: api/Programs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProgramEntity>> GetProgramEntity(int id)
        {
            var programEntity = await _context.Programs.FindAsync(id);

            if (programEntity == null)
            {
                return NotFound();
            }

            return programEntity;
        }

        // PUT: api/Programs/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProgramEntity(int id, ProgramEntity programEntity)
        {
            if (id != programEntity.Id)
            {
                return BadRequest();
            }

            _context.Entry(programEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgramEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Programs
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ProgramEntity>> PostProgramEntity(ProgramEntity programEntity)
        {
            _context.Programs.Add(programEntity);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProgramEntity", new { id = programEntity.Id }, programEntity);
        }

        // DELETE: api/Programs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProgramEntity(int id)
        {
            var programEntity = await _context.Programs.FindAsync(id);
            if (programEntity == null)
            {
                return NotFound();
            }

            _context.Programs.Remove(programEntity);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ProgramEntityExists(int id)
        {
            return _context.Programs.Any(e => e.Id == id);
        }
    }
}