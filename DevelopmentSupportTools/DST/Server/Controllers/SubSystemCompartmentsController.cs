﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DST.Server.Data;
using DST.Shared.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DST.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubSystemCompartmentsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public SubSystemCompartmentsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/SubSystemCompartments
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SubSystemCompartmentEntity>>> GetSubSystemCompartments()
        {
            return await _context.SubSystemCompartments
                .Include(s => s.SystemCompartment)
                .OrderBy(s => s.SystemCompartment.Compartment)
                .ThenBy(s => s.Compartment)
                .ToListAsync();
        }

        // GET: api/SubSystemCompartments/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SubSystemCompartmentEntity>> GetSubSystemCompartmentEntity(int id)
        {
            var subSystemCompartmentEntity = await _context.SubSystemCompartments.FindAsync(id);

            if (subSystemCompartmentEntity == null)
            {
                return NotFound();
            }

            return subSystemCompartmentEntity;
        }

        // PUT: api/SubSystemCompartments/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubSystemCompartmentEntity(int id, SubSystemCompartmentEntity subSystemCompartmentEntity)
        {
            if (id != subSystemCompartmentEntity.Id)
            {
                return BadRequest();
            }

            _context.Entry(subSystemCompartmentEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubSystemCompartmentEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SubSystemCompartments
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<SubSystemCompartmentEntity>> PostSubSystemCompartmentEntity(SubSystemCompartmentEntity subSystemCompartmentEntity)
        {
            _context.SubSystemCompartments.Add(subSystemCompartmentEntity);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSubSystemCompartmentEntity", new { id = subSystemCompartmentEntity.Id }, subSystemCompartmentEntity);
        }

        // DELETE: api/SubSystemCompartments/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSubSystemCompartmentEntity(int id)
        {
            var subSystemCompartmentEntity = await _context.SubSystemCompartments.FindAsync(id);
            if (subSystemCompartmentEntity == null)
            {
                return NotFound();
            }

            _context.SubSystemCompartments.Remove(subSystemCompartmentEntity);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool SubSystemCompartmentEntityExists(int id)
        {
            return _context.SubSystemCompartments.Any(e => e.Id == id);
        }
    }
}