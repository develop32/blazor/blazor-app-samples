﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using BlazorDownloadFile;
using Microsoft.AspNetCore.Components;
using VMS.Domain.Dtos;
using VMS.Domain.Entities;
using VMS.Domain.Repositories;

namespace VMS.Server.Pages
{
    /// <summary>
    /// 来客申請一覧
    /// </summary>
    public partial class ApplicationList : ComponentBase
    {
        private readonly ApplicationListSearchItemDto searchItem = new();
        private readonly ApplicationEntity tableHeader = new();
        private IEnumerable<ApplicationEntity> applications = new List<ApplicationEntity>();

        /// <summary>ファイルダウンロードサービス</summary>
        [Inject]
        protected IBlazorDownloadFileService BlazorDownloadFileService { get; set; }

        /// <summary>来客申請レポジトリ</summary>
        [Inject]
        protected IApplicationsRepository ApplicationsRepository { get; set; }

        /// <summary>
        /// 初期処理
        /// </summary>
        /// <returns>非同期</returns>
        protected override async Task OnInitializedAsync()
        {
            await Search();
        }

        private async Task Search()
        {
            try
            {
                CustomLogger.Debug("検索ボタンがクリックされました。");
                applications = await ApplicationsRepository.GetApplications(searchItem);
            }
            catch (Exception ex)
            {
                CustomLogger.Error(ex.Message);
            }
        }

        private async Task DownloadFileExec_Excel()
        {
            var filename = "来客計画テンプレート.xlsx";

            var path = Path.Combine(Domain.Shared.DownloadFolder, filename);

            byte[] data = File.ReadAllBytes(path);

            await BlazorDownloadFileService.DownloadFile(filename, data, contentType: "application/octet-stream");
        }
    }
}