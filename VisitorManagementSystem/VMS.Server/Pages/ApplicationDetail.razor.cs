﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using VMS.Domain.Dtos;
using VMS.Domain.Entities;
using VMS.Domain.Repositories;
using VMS.Domain.ValueObjects;

namespace VMS.Server.Pages
{
    /// <summary>
    /// 来客申請詳細
    /// </summary>
    public partial class ApplicationDetail : ComponentBase
    {
        private readonly VisitorEntity visitorsHeader = new();
        private readonly ScheduleEntity scheduleHeader = new();
        private ApplicationDetailMode applicationDetailMode;
        private ApplicationDetailDto application = new();
        private List<UserEntity> users = new();

        private string selectedUserError = string.Empty;
        private UserEntity _selectedUser = null;

        /// <summary>ID</summary>
        [Parameter]
        public int Id { get; set; }

        /// <summary>呼び出し元画面で押下したボタン</summary>
        [Parameter]
        public string ApplicationDetailModeValue { get; set; }

        /// <summary>ナビゲーションマネージャー</summary>
        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        /// <summary>スナックバー</summary>
        [Inject]
        protected ISnackbar Snackbar { get; set; }

        /// <summary>来客申請レポジトリ</summary>
        [Inject]
        protected IApplicationsRepository ApplicationsRepository { get; set; }

        private UserEntity SelectedUser
        {
            get
            {
                return _selectedUser;
            }

            set
            {
                _selectedUser = value;
                if (_selectedUser != null && users.Any(u => u.Id == _selectedUser.Id))
                {
                    selectedUserError = "このユーザは追加済みです。";
                }
                else
                {
                    selectedUserError = string.Empty;
                }
            }
        }

        /// <summary>
        /// パラメータを受け取ったときの処理。
        /// </summary>
        /// <returns>非同期操作を表すTask。</returns>
        protected override async Task OnParametersSetAsync()
        {
            applicationDetailMode = new(ApplicationDetailModeValue);
            if (applicationDetailMode != ApplicationDetailMode.Create)
            {
                application = await ApplicationsRepository.GetApplication(Id);
            }
        }

        private void AddUser()
        {
            users.Add(SelectedUser);
            SelectedUser = SelectedUser;
        }

        private async Task SaveTemporarily()
        {
            try
            {
                if (applicationDetailMode == ApplicationDetailMode.Create)
                {
                    ApplicationDetailDto addedApplication = await ApplicationsRepository.CreateApplication(ApplicationDetailButton.SaveTemporarily, application);
                    Snackbar.Add("一時保存しました。", Severity.Success);
                    NavigationManager.NavigateTo($"application-detail/{ApplicationDetailMode.Edit.Value}/{addedApplication.Id}");
                }
                else if (applicationDetailMode == ApplicationDetailMode.Edit)
                {
                    ApplicationDetailDto addedApplication = await ApplicationsRepository.UpdateApplication(ApplicationDetailButton.SaveTemporarily, Id, application);
                    Snackbar.Add("一時保存しました。", Severity.Success);
                }
            }
            catch (Exception ex)
            {
                string message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                Snackbar.Add(message, Severity.Error);
                CustomLogger.Error(message, stackTrace: ex.StackTrace);
            }
        }

        private async void Delete()
        {
            try
            {
                await ApplicationsRepository.DeleteApplication(Id);
                Snackbar.Add("削除しました。", Severity.Success);
                NavigationManager.NavigateTo("application-list");
            }
            catch (Exception ex)
            {
                string message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                Snackbar.Add(message, Severity.Error);
                CustomLogger.Error(message, stackTrace: ex.StackTrace);
            }
        }

        private void Cancel()
        {
            NavigationManager.NavigateTo("application-list");
        }
    }
}