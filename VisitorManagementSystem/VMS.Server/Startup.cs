﻿using System;
using System.Runtime.InteropServices;
using BlazorDownloadFile;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MudBlazor;
using MudBlazor.Services;
using Toolbelt.Blazor.Extensions.DependencyInjection;
using VMS.Domain.Modules;
using VMS.Domain.Repositories;
using VMS.Infrastructure.Oracle;

namespace VMS.Server
{
    /// <summary>
    /// スタートアップクラス
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="configuration">設定</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>設定</summary>
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940

        /// <summary>
        /// 設定サービス
        /// </summary>
        /// <param name="services">サービス</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.EnableSensitiveDataLogging();
                options.UseOracle(Configuration.GetConnectionString("DefaultConnection"));
                options.EnableDetailedErrors(true);
            });

            services.AddBlazorDownloadFile();
            services.AddMudServices(config =>
            {
                config.SnackbarConfiguration.PositionClass = Defaults.Classes.Position.TopCenter;
            });

            services.AddScoped<IApplicationsRepository, ApplicationsOracle>();
            services.AddScoped<IUsersRepository, UsersOracle>();
            services.AddScoped<ILogsRepository, LogsOracle>();

            services.AddScoped<ICustomLogger, CustomLogger>();

            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddMudServices();
            services.AddHeadElementHelper();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.

        /// <summary>
        /// 構成
        /// </summary>
        /// <param name="app">アプリケーション</param>
        /// <param name="env">環境</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                // on Windows
                Domain.Shared.DownloadFolder = env.WebRootPath + "\\..\\Download";
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                // on Linux
                Domain.Shared.DownloadFolder = env.WebRootPath + "/../Download";
            }
            else
            {
                throw new NotImplementedException();
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");

                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}