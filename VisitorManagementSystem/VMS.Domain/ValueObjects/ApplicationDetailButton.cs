﻿namespace VMS.Domain.ValueObjects
{
    /// <summary>
    /// 来客申請詳細ボタン値オブジェクト
    /// </summary>
    public class ApplicationDetailButton : AbstractValueObject<ApplicationDetailButton>
    {
        /// <summary>一時保存</summary>
        public static readonly ApplicationDetailButton SaveTemporarily = new("SaveTemporarily");

        /// <summary>提出</summary>
        public static readonly ApplicationDetailButton Submit = new("Submit");

        /// <summary>承認</summary>
        public static readonly ApplicationDetailButton Approve = new("Approve");

        /// <summary>否認</summary>
        public static readonly ApplicationDetailButton Deny = new("Deny");

        /// <summary>取消し</summary>
        public static readonly ApplicationDetailButton Cancel = new("Cancel");

        /// <summary>取戻し</summary>
        public static readonly ApplicationDetailButton TakeBack = new("TakeBack");

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="value">値</param>
        public ApplicationDetailButton(string value)
        {
            Value = value;
        }

        /// <summary>値</summary>
        public string Value { get; set; }

        /// <inheritdoc/>
        protected override bool EqualsCore(ApplicationDetailButton other)
        {
            return Value == other.Value;
        }
    }
}