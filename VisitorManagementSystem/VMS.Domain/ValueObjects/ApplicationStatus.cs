﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace VMS.Domain.ValueObjects
{
    /// <summary>
    /// 来客申請ステータス値オブジェクト
    /// </summary>
    [Owned]
    public class ApplicationStatus : AbstractValueObject<ApplicationStatus>
    {
        /// <summary>一時保存</summary>
        public static readonly ApplicationStatus None = new(0);

        /// <summary>一時保存</summary>
        public static readonly ApplicationStatus TemporarySaving = new(1);

        /// <summary>承認1待ち</summary>
        public static readonly ApplicationStatus WaitingForApproval1 = new(2);

        /// <summary>承認2待ち</summary>
        public static readonly ApplicationStatus WaitingForApproval2 = new(3);

        /// <summary>承認3待ち</summary>
        public static readonly ApplicationStatus WaitingForApproval3 = new(4);

        /// <summary>承認済み</summary>
        public static readonly ApplicationStatus Approved = new(5);

        /// <summary>取消済み</summary>
        public static readonly ApplicationStatus Cancelled = new(6);

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="value">値</param>
        public ApplicationStatus(int value)
        {
            Value = value;
        }

        /// <summary>値</summary>
        [Comment("来客申請ステータス値:一時保存(1)、承認1待ち(2)、承認2待ち(3)、承認3待ち(4)、承認済み(5)、取消済み(6)")]
        [Column("ApplicationStatus")]
        public int Value { get; set; }

        /// <summary>表示値</summary>
        [Display(Name = "来客申請ステータス")]
        public string DisplayValue
        {
            get
            {
                if (this == TemporarySaving)
                    return "一時保存";
                if (this == WaitingForApproval1)
                    return "承認1待ち";
                if (this == WaitingForApproval2)
                    return "承認2待ち";
                if (this == WaitingForApproval3)
                    return "承認3待ち";
                if (this == Approved)
                    return "承認済み";
                if (this == Cancelled)
                    return "取消済み";
                return "不明";
            }
        }

        /// <summary>
        /// 来客申請ステータス一覧を返します。
        /// </summary>
        /// <returns>来客申請ステータス一覧</returns>
        public static List<ApplicationStatus> ToList()
        {
            return new List<ApplicationStatus>()
            {
                TemporarySaving,
                WaitingForApproval1,
                WaitingForApproval2,
                WaitingForApproval3,
                Approved,
                Cancelled,
            };
        }

        /// <summary>
        /// 比較
        /// </summary>
        /// <param name="other">他の値オブジェクト</param>
        /// <returns>Trueの場合、等しい</returns>
        protected override bool EqualsCore(ApplicationStatus other)
        {
            return Value == other.Value;
        }
    }
}