﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace VMS.Domain.ValueObjects
{
    /// <summary>
    /// 送信先区分値オブジェクト
    /// </summary>
    [Owned]
    public class DestinationType
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="value">値</param>
        public DestinationType(int value)
        {
            Value = value;
        }

        /// <summary>値</summary>
        [Comment("送信先区分:To(1)、CC(2)、BCC(3)")]
        [Column("DestinationType")]
        public int Value { get; set; }
    }
}