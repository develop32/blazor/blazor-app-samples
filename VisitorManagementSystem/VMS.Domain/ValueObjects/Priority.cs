﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace VMS.Domain.ValueObjects
{
    /// <summary>
    /// 優先度値オブジェクト
    /// </summary>
    [Owned]
    public class Priority : AbstractValueObject<Priority>
    {
        /// <summary>低(1)</summary>
        public static readonly Priority Low = new(1);

        /// <summary>普通(2)</summary>
        public static readonly Priority Normal = new(2);

        /// <summary>高(3)</summary>
        public static readonly Priority High = new(3);

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="value">値</param>
        public Priority(int value)
        {
            Value = value;
        }

        /// <summary>値</summary>
        [Comment("優先度:低(1)、普通(2)、高(3)")]
        [Column("Priority")]
        public int Value { get; set; }

        /// <inheritdoc/>
        protected override bool EqualsCore(Priority other)
        {
            StringBuilder aaa = new();

            return Value == other.Value;
        }
    }
}