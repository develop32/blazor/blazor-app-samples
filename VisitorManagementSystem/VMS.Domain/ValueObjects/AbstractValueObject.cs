﻿namespace VMS.Domain.ValueObjects
{
    /// <summary>
    /// 基底値オブジェクトクラス
    /// </summary>
    /// <typeparam name="T">タイプ</typeparam>
    public abstract class AbstractValueObject<T>
        where T : AbstractValueObject<T>
    {
        /// <summary>
        /// ＝
        /// </summary>
        /// <param name="vo1">値オブジェクト1</param>
        /// <param name="vo2">値オブジェクト2</param>
        /// <returns>Trueの場合、等しい</returns>
        public static bool operator ==(AbstractValueObject<T> vo1, AbstractValueObject<T> vo2)
        {
            return Equals(vo1, vo2);
        }

        /// <summary>
        /// 不等号
        /// </summary>
        /// <param name="vo1">値オブジェクト1</param>
        /// <param name="vo2">値オブジェクト2</param>
        /// <returns>Trueの場合、等しくない</returns>
        public static bool operator !=(AbstractValueObject<T> vo1, AbstractValueObject<T> vo2)
        {
            return !Equals(vo1, vo2);
        }

        /// <summary>
        /// Trueの場合、等しい。
        /// </summary>
        /// <param name="obj">オブジェクト</param>
        /// <returns>Trueの場合、等しい</returns>
        public override bool Equals(object obj)
        {
            var vo = obj as T;
            if (vo == null)
            {
                return false;
            }

            return EqualsCore(vo);
        }

        /// <summary>
        /// 文字列へ変換する。
        /// </summary>
        /// <returns>文字列</returns>
        public override string ToString()
        {
            return base.ToString();
        }

        /// <summary>
        /// ハッシュコードを取得する。
        /// </summary>
        /// <returns>ハッシュコード</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// 比較
        /// </summary>
        /// <param name="other">他の値オブジェクト</param>
        /// <returns>Trueの場合、等しい。</returns>
        protected abstract bool EqualsCore(T other);
    }
}