﻿namespace VMS.Domain.ValueObjects
{
    /// <summary>
    /// 来客申請詳細モード値オブジェクト
    /// </summary>
    public class ApplicationDetailMode : AbstractValueObject<ApplicationDetailMode>
    {
        /// <summary>新規作成</summary>
        public static readonly ApplicationDetailMode Create = new("Create");

        /// <summary>照会</summary>
        public static readonly ApplicationDetailMode Show = new("Show");

        /// <summary>編集</summary>
        public static readonly ApplicationDetailMode Edit = new("Edit");

        /// <summary>削除</summary>
        public static readonly ApplicationDetailMode Delete = new("Delete");

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="value">値</param>
        public ApplicationDetailMode(string value)
        {
            Value = value;
        }

        /// <summary>値</summary>
        public string Value { get; set; }

        /// <inheritdoc/>
        protected override bool EqualsCore(ApplicationDetailMode other)
        {
            return Value == other.Value;
        }
    }
}