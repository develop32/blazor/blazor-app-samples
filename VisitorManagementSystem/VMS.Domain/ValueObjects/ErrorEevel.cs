﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace VMS.Domain.ValueObjects
{
    /// <summary>
    /// エラーレベル値オブジェクト
    /// </summary>
    [Owned]
    public class ErrorEevel : AbstractValueObject<ErrorEevel>
    {
        /// <summary>
        /// 1:デバッグ。開発のデバッグ時に必要な情報。
        /// </summary>
        public static readonly ErrorEevel Debug = new("Debug");

        /// <summary>2:情報</summary>
        public static readonly ErrorEevel Info = new("Info");

        /// <summary>3:警告</summary>
        public static readonly ErrorEevel Warn = new("Warn");

        /// <summary>4:エラー</summary>
        public static readonly ErrorEevel Error = new("Error");

        /// <summary>5:重大なエラー</summary>
        public static readonly ErrorEevel Fatal = new("Fatal");

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="value">値</param>
        public ErrorEevel(string value)
        {
            Value = value;
        }

        /// <summary>値</summary>
        [Comment("エラーレベル")]
        [Column("ErrorEevel")]
        [MaxLength(10)]
        public string Value { get; set; }

        /// <inheritdoc/>
        protected override bool EqualsCore(ErrorEevel other)
        {
            return Value == other.Value;
        }
    }
}