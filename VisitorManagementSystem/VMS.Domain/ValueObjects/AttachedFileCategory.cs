﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace VMS.Domain.ValueObjects
{
    /// <summary>
    /// 来客申請ステータス値オブジェクト
    /// </summary>
    [Owned]
    public class AttachedFileCategory : AbstractValueObject<AttachedFileCategory>
    {
        /// <summary>来客リスト</summary>
        public static readonly AttachedFileCategory VisitorList = new(1);

        /// <summary>チェックシート</summary>
        public static readonly AttachedFileCategory CheckSheet = new(2);

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="value">値</param>
        public AttachedFileCategory(int value)
        {
            Value = value;
        }

        /// <summary>値</summary>
        [Column("AttachedFileCategory")]
        [Comment("添付ファイル区分:来客リスト(1)、チェックシート(2)")]
        public int Value { get; set; }

        /// <summary>表示値</summary>
        public string DisplayValue
        {
            get
            {
                if (this == VisitorList)
                    return "来客者リスト";
                if (this == CheckSheet)
                    return "チェックシート";
                return "不明";
            }
        }

        /// <inheritdoc/>
        protected override bool EqualsCore(AttachedFileCategory other)
        {
            return Value == other.Value;
        }
    }
}
