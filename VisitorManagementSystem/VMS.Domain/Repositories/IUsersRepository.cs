﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VMS.Domain.Entities;

namespace VMS.Domain.Repositories
{
    /// <summary>
    /// ユーザレポジトリ
    /// </summary>
    public interface IUsersRepository
    {
        /// <summary>
        /// すべてのユーザを取得します。
        /// </summary>
        /// <returns>すべてのユーザ</returns>
        public Task<IEnumerable<UserEntity>> GetAllUsers();
    }
}