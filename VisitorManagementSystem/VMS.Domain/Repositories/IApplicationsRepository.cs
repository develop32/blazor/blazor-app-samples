﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VMS.Domain.Dtos;
using VMS.Domain.Entities;
using VMS.Domain.ValueObjects;

namespace VMS.Domain.Repositories
{
    /// <summary>
    /// 来客申請レポジトリ
    /// </summary>
    public interface IApplicationsRepository
    {
        /// <summary>
        /// 来客申請を取得します。
        /// </summary>
        /// <param name="id">来客申請ID</param>
        /// <returns>来客申請</returns>
        public Task<ApplicationDetailDto> GetApplication(int id);

        /// <summary>
        /// 来客申請一覧を取得します。
        /// </summary>
        /// <param name="searchItem">検索項目</param>
        /// <returns>来客申請一覧</returns>
        public Task<IEnumerable<ApplicationEntity>> GetApplications(ApplicationListSearchItemDto searchItem);

        /// <summary>
        /// 来客申請を新規登録します。
        /// </summary>
        /// <param name="button">ボタン</param>
        /// <param name="application">来客申請</param>
        /// <returns>更新レコード数</returns>
        public Task<ApplicationDetailDto> CreateApplication(ApplicationDetailButton button, ApplicationDetailDto application);

        /// <summary>
        /// 来客申請を更新します。
        /// </summary>
        /// <param name="button">ボタン</param>
        /// <param name="id">来客申請ID</param>
        /// <param name="application">来客申請</param>
        /// <returns>更新レコード数</returns>
        public Task<ApplicationDetailDto> UpdateApplication(ApplicationDetailButton button, int id, ApplicationDetailDto application);

        /// <summary>
        /// 来客申請を削除します。
        /// </summary>
        /// <param name="id">来客申請ID</param>
        /// <returns>非同期処理</returns>
        public Task DeleteApplication(int id);
    }
}