﻿using System.Threading.Tasks;
using VMS.Domain.Entities;

namespace VMS.Domain.Repositories
{
    /// <summary>
    /// ログレポジトリ
    /// </summary>
    public interface ILogsRepository
    {
        /// <summary>
        /// ログを追加します。
        /// </summary>
        /// <param name="log">ログ情報</param>
        /// <returns>非同期処理</returns>
        public Task AddLog(LogEntity log);
    }
}