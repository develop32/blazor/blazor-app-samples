﻿using System.Threading.Tasks;

namespace VMS.Domain.Repositories
{
    /// <summary>
    /// 送信メールレポジトリ
    /// </summary>
    public interface ISendingMailRepository
    {
        /// <summary>
        /// 送信メール情報を登録します。
        /// </summary>
        /// <returns>ID</returns>
        public Task<int> CreateSendingMail();
    }
}