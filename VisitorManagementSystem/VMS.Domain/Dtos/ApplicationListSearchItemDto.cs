﻿using System.ComponentModel.DataAnnotations;

namespace VMS.Domain.Dtos
{
    /// <summary>
    /// 来客申請一覧検索項目DTOクラス
    /// </summary>
    public class ApplicationListSearchItemDto
    {
        /// <summary>目的</summary>
        [Display(Name = "目的")]
        public string Purpose { get; set; } = string.Empty;
    }
}