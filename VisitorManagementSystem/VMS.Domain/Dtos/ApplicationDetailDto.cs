﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using VMS.Domain.Entities;

namespace VMS.Domain.Dtos
{
    /// <summary>
    /// 来客申請詳細ページDTOクラス
    /// </summary>
    public class ApplicationDetailDto
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ApplicationDetailDto()
        {
            FromDatetime = DateTime.Now.Date;
            FromTime = new TimeSpan(9, 0, 0);
            ToDatetime = DateTime.Now.Date;
            ToTime = new TimeSpan(17, 0, 0);
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="application">来客申請</param>
        public ApplicationDetailDto(ApplicationEntity application)
        {
            Id = application.Id;
            Purpose = application.Purpose;
            FromDatetime = application.FromDatetime;
            if (application.FromDatetime != null)
            {
                FromTime = new TimeSpan(
                    (int)application.FromDatetime?.Hour,
                    (int)application.FromDatetime?.Minute,
                    (int)application.FromDatetime?.Second);
            }

            ToDatetime = application.ToDatetime;

            if (application.ToDatetime != null)
            {
                ToTime = new TimeSpan(
                    (int)application.ToDatetime?.Hour,
                    (int)application.ToDatetime?.Minute,
                    (int)application.ToDatetime?.Second);
            }

            Schedules = application.Schedules.ToList();
            Visitors = application.Visitors.ToList();
        }

        /// <summary>Id</summary>
        [Display(Name = "Id")]
        public int Id { get; set; }

        /// <summary>目的</summary>
        [Display(Name = "目的")]
        [Required(ErrorMessage = "{0}は入力必須です。")]
        [MaxLength(50, ErrorMessage = "{0}は{1}文字までです。")]
        public string Purpose { get; set; } = string.Empty;

        /// <summary>開始日付</summary>
        [Display(Name = "開始日付")]
        [Required(ErrorMessage = "{0}は入力必須です。")]
        public DateTime? FromDatetime { get; set; }

        /// <summary>開始時刻</summary>
        [Display(Name = "開始時刻")]
        [Required(ErrorMessage = "{0}は入力必須です。")]
        public TimeSpan? FromTime { get; set; }

        /// <summary>終了日付</summary>
        [Display(Name = "終了日付")]
        [Required(ErrorMessage = "{0}は入力必須です。")]
        public DateTime? ToDatetime { get; set; }

        /// <summary>終了時刻</summary>
        [Display(Name = "終了時刻")]
        [Required(ErrorMessage = "{0}は入力必須です。")]
        public TimeSpan? ToTime { get; set; }

        /// <summary>スケジュール情報</summary>
        [Display(Name = "スケジュール情報")]
        public List<ScheduleEntity> Schedules { get; set; } = new List<ScheduleEntity>();

        /// <summary>来客者情報</summary>
        [Display(Name = "来客者情報")]
        public List<VisitorEntity> Visitors { get; set; } = new List<VisitorEntity>();

        /// <summary>添付ファイル情報</summary>
        [Display(Name = "添付ファイル情報")]
        public List<AttachedFileEntity> AttachedFiles { get; set; } = new List<AttachedFileEntity>();
    }
}