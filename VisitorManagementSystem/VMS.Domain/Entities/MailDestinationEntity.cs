﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using VMS.Domain.ValueObjects;

namespace VMS.Domain.Entities
{
    /// <summary>
    /// メール送信先エンティティクラス
    /// </summary>
    [Table("MailDestinations")]
    [Comment("メール送信先")]
    public class MailDestinationEntity : AbstractEntity
    {
        /// <summary>送信メールID</summary>
        [Display(Name = "送信メールID")]
        [Comment("送信メールID(FK)")]
        public int SendingMailId { get; set; }

        /// <summary>送信先区分</summary>
        public DestinationType DestinationType { get; set; }

        /// <summary>メールアドレス</summary>
        [Display(Name = "メールアドレス")]
        [Comment("メールアドレス")]
        public string MailAddress { get; set; }

        /// <summary>送信メール情報</summary>
        [Display(Name = "送信メール情報")]
        [ForeignKey(nameof(SendingMailId))]
        [InverseProperty(nameof(SendingMailEntity.MailDestinations))]
        public virtual SendingMailEntity SendingMail { get; set; }
    }
}