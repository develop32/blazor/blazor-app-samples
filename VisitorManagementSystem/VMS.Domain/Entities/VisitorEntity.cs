﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace VMS.Domain.Entities
{
    /// <summary>
    /// 来客者エンティティクラス
    /// </summary>
    [Table("Visitors")]
    [Comment("来客者")]
    public class VisitorEntity : AbstractEntity
    {
        /// <summary>来客申請ID</summary>
        [Display(Name = "来客申請ID")]
        [Comment("来客申請ID")]
        public int ApplicationId { get; set; }

        /// <summary>会社名</summary>
        [Display(Name = "会社名")]
        [Comment("会社名")]
        [MaxLength(50, ErrorMessage = "{0}は{1}文字までです。")]
        public string CompanyName { get; set; }

        /// <summary>名前</summary>
        [Display(Name = "名前")]
        [Comment("名前")]
        [MaxLength(50, ErrorMessage = "{0}は{1}文字までです。")]
        public string Name { get; set; }

        /// <summary>
        /// Trueの場合、未入力です。
        /// </summary>
        public bool IsBlank
        {
            get
            {
                if (!string.IsNullOrEmpty(CompanyName)) return false;
                if (!string.IsNullOrEmpty(Name)) return false;

                return true;
            }
        }

        /// <summary>来客申請情報</summary>
        [Display(Name = "来客申請情報")]
        [ForeignKey(nameof(ApplicationId))]
        [InverseProperty(nameof(ApplicationEntity.Visitors))]
        public virtual ApplicationEntity Application { get; set; }
    }
}