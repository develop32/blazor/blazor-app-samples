﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace VMS.Domain.Entities
{
    /// <summary>
    /// スケジュールエンティティクラス
    /// </summary>
    [Table("Schedules")]
    [Comment("スケジュール")]
    public class ScheduleEntity : AbstractEntity
    {
        private TimeSpan? _fromTime;
        private TimeSpan? _toTime;

        /// <summary>来客申請ID</summary>
        [Display(Name = "来客申請ID")]
        [Comment("来客申請ID")]
        public int ApplicationId { get; set; }

        /// <summary>開始日時</summary>
        [Display(Name = "開始日時")]
        [Comment("開始日時")]
        public DateTime? FromDatetime { get; set; }

        /// <summary>開始時刻</summary>
        [NotMapped]
        [Display(Name = "開始時刻")]
        public TimeSpan? FromTime
        {
            get
            {
                return _fromTime;
            }

            set
            {
                _fromTime = value;
                FromDatetime = new DateTime(
                    (int)FromDatetime?.Year,
                    (int)FromDatetime?.Month,
                    (int)FromDatetime?.Day,
                    (int)_fromTime?.Hours,
                    (int)_fromTime?.Minutes,
                    (int)_fromTime?.Seconds);
            }
        }

        /// <summary>終了日時</summary>
        [Display(Name = "終了日時")]
        [Comment("終了日時")]
        public DateTime? ToDatetime { get; set; }

        /// <summary>終了時刻</summary>
        [NotMapped]
        [Display(Name = "終了時刻")]
        public TimeSpan? ToTime
        {
            get
            {
                return _toTime;
            }

            set
            {
                _toTime = value;
                ToDatetime = new DateTime(
                    (int)ToDatetime?.Year,
                    (int)ToDatetime?.Month,
                    (int)ToDatetime?.Day,
                    (int)_toTime?.Hours,
                    (int)_toTime?.Minutes,
                    (int)_toTime?.Seconds);
            }
        }

        /// <summary>概要</summary>
        [Display(Name = "概要")]
        [Comment("概要")]
        [Required]
        [MaxLength(50, ErrorMessage = "{0}は{1}文字までです。")]
        public string Overview { get; set; }

        /// <summary>詳細</summary>
        [Display(Name = "詳細")]
        [Comment("詳細")]
        [MaxLength(100, ErrorMessage = "{0}は{1}文字までです。")]
        public string Details { get; set; }

        /// <summary>
        /// Trueの場合、未入力です。
        /// </summary>
        public bool IsBlank
        {
            get
            {
                if (FromDatetime != null) return false;
                if (ToDatetime != null) return false;
                if (!string.IsNullOrEmpty(Overview)) return false;
                if (!string.IsNullOrEmpty(Details)) return false;

                return true;
            }
        }

        /// <summary>来客申請情報</summary>
        [Display(Name = "来客申請情報")]
        [ForeignKey(nameof(ApplicationId))]
        [InverseProperty(nameof(ApplicationEntity.Schedules))]
        public virtual ApplicationEntity Application { get; set; }
    }
}