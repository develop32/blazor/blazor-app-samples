﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace VMS.Domain.Entities
{
    /// <summary>
    /// 基底エンティティクラス
    /// </summary>
    public class AbstractEntity : IAbstractEntity
    {
        /// <summary>ID</summary>
        [Display(Name = "ID")]
        [Comment("ID:自動採番。")]
        [Key]
        public int Id { get; set; }

        /// <summary>作成者</summary>
        [Display(Name = "作成者")]
        [Comment("作成者")]
        [Required]
        public int CreatedBy { get; set; }

        /// <summary>作成日時</summary>
        [Display(Name = "作成日時")]
        [Comment("作成日時")]
        [Required]
        public DateTime CreatedOn { get; set; }

        /// <summary>更新者</summary>
        [Display(Name = "更新者")]
        [Comment("更新者")]
        public int? UpdatedBy { get; set; }

        /// <summary>更新日時</summary>
        [Display(Name = "更新日時")]
        [Comment("更新日時")]
        public DateTime? UpdatedOn { get; set; }
    }
}