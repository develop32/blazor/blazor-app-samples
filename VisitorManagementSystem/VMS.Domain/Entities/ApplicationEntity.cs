﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using VMS.Domain.ValueObjects;

namespace VMS.Domain.Entities
{
    /// <summary>
    /// 来客申請エンティティクラス
    /// </summary>
    [Table("Applications")]
    [Comment("来客申請")]
    public class ApplicationEntity : AbstractEntity
    {
        /// <summary>目的</summary>
        [Display(Name = "目的")]
        [Comment("目的")]
        [Required]
        [MaxLength(50, ErrorMessage = "{0}は{1}文字までです。")]
        public string Purpose { get; set; }

        /// <summary>開始日時</summary>
        [Display(Name = "開始日時")]
        [Comment("開始日時")]
        [Required]
        public DateTime? FromDatetime { get; set; }

        /// <summary>終了日時</summary>
        [Display(Name = "終了日時")]
        [Comment("終了日時")]
        [Required]
        public DateTime? ToDatetime { get; set; }

        /// <summary>来客申請ステータス</summary>
        [Display(Name = "来客申請ステータス")]
        public virtual ApplicationStatus ApplicationStatus { get; set; }

        /// <summary>来客者情報</summary>
        [Display(Name = "来客者情報")]
        [Comment("来客者情報")]
        [InverseProperty(nameof(VisitorEntity.Application))]
        public virtual ICollection<VisitorEntity> Visitors { get; set; }

        /// <summary>スケジュール情報</summary>
        [Display(Name = "スケジュール情報")]
        [InverseProperty(nameof(ScheduleEntity.Application))]
        public virtual ICollection<ScheduleEntity> Schedules { get; set; }

        /// <summary>添付ファイル情報</summary>
        [Display(Name = "添付ファイル情報")]
        [InverseProperty(nameof(AttachedFileEntity.Application))]
        public virtual ICollection<AttachedFileEntity> AttachedFiles { get; set; }
    }
}