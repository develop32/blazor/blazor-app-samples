﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using VMS.Domain.ValueObjects;

namespace VMS.Domain.Entities
{
    /// <summary>
    /// 添付ファイルエンティティクラス
    /// </summary>
    [Table("AttachedFiles")]
    [Comment("添付ファイル")]
    public class AttachedFileEntity : AbstractEntity
    {
        /// <summary>来客申請ID</summary>
        [Display(Name = "来客申請ID")]
        [Comment("来客申請ID")]
        public int ApplicationId { get; set; }

        /// <summary>添付ファイル名</summary>
        [Display(Name = "添付ファイル名")]
        [Comment("添付ファイル名")]
        public string Name { get; set; }

        /// <summary>値</summary>
        [Display(Name = "添付ファイル区分")]
        public AttachedFileCategory AttachedFileCategory { get; set; }

        /// <summary>コンテントタイプ（MIME の種類）</summary>
        [Display(Name = "コンテントタイプ")]
        [Comment("コンテントタイプ")]
        public string ContentType { get; set; }

        /// <summary>コンテンツ</summary>
        [Display(Name = "コンテンツ")]
        [Comment("コンテンツ")]
        [MaxLength(20000)]
        public byte[] Contents { get; set; }

        /// <summary>最終変更日</summary>
        [Display(Name = "最終変更日")]
        [Comment("最終変更日")]
        public DateTimeOffset LastModified { get; set; }

        /// <summary>サイズ</summary>
        [Display(Name = "サイズ（バイト単位）")]
        [Comment("サイズ（バイト単位）")]
        public long Size { get; set; }

        /// <summary>来客申請情報</summary>
        [Display(Name = "来客申請情報")]
        [ForeignKey(nameof(ApplicationId))]
        [InverseProperty(nameof(ApplicationEntity.AttachedFiles))]
        public virtual ApplicationEntity Application { get; set; }
    }
}