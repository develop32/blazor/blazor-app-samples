﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using VMS.Domain.ValueObjects;

namespace VMS.Domain.Entities
{
    /// <summary>
    /// 送信メールエンティティクラス
    /// </summary>
    [Table("SendingMails")]
    [Comment("送信メール")]
    public class SendingMailEntity : AbstractEntity
    {
        /// <summary>件名</summary>
        [Display(Name = "件名")]
        [Comment("件名")]
        [StringLength(50)]
        public string Subject { get; set; }

        /// <summary>本文</summary>
        [Display(Name = "本文")]
        [Comment("本文")]
        public string Body { get; set; }

        /// <summary>優先度</summary>
        public Priority Priority { get; set; }

        /// <summary>メール送信先情報</summary>
        [Display(Name = "メール送信先情報")]
        [InverseProperty(nameof(MailDestinationEntity.SendingMail))]
        public virtual ICollection<MailDestinationEntity> MailDestinations { get; set; }
    }
}