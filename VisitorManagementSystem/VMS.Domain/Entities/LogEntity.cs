﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using VMS.Domain.ValueObjects;

namespace VMS.Domain.Entities
{
    /// <summary>
    /// ログエンティティクラス
    /// </summary>
    [Table("Logs")]
    [Comment("ログ")]
    public class LogEntity : AbstractEntity
    {
        /// <summary>エラーレベル</summary>
        [Display(Name = "エラーレベル")]
        public ErrorEevel ErrorEevel { get; set; }

        /// <summary>メッセージ</summary>
        [Display(Name = "メッセージ")]
        [Comment("メッセージ")]
        [Required]
        public string Message { get; set; }

        /// <summary>データ</summary>
        [Display(Name = "データ")]
        [Comment("データ")]
        public string Data { get; set; }

        /// <summary>ファイル名</summary>
        [Display(Name = "ファイル名")]
        [Comment("ファイル名")]
        public string FileName { get; set; }

        /// <summary>メンバ名</summary>
        [Display(Name = "メンバ名")]
        [Comment("メンバ名")]
        public string MemberName { get; set; }

        /// <summary>ラインNo</summary>
        [Display(Name = "ラインNo")]
        [Comment("ラインNo")]
        public int? LineNo { get; set; }

        /// <summary>スタックトレース</summary>
        [Display(Name = "スタックトレース")]
        [Comment("スタックトレース")]
        [MaxLength(2001)]
        public string StackTrace { get; set; }

        /// <summary>SQL</summary>
        [Display(Name = "SQL")]
        [Comment("SQL")]
        [MaxLength(2001)]
        public string Sql { get; set; }

        /// <summary>備考</summary>
        [Display(Name = "備考")]
        [Comment("備考")]
        public string Remarks { get; set; }
    }
}