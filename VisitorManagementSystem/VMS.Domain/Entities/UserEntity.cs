﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace VMS.Domain.Entities
{
    /// <summary>
    /// ユーザエンティティクラス
    /// </summary>
    [Table("Users")]
    [Comment("ユーザ")]
    public class UserEntity : AbstractEntity
    {
        /// <summary>ユーザ名</summary>
        [Display(Name = "ユーザ名")]
        [Comment("ユーザ名")]
        [Required]
        [MaxLength(30, ErrorMessage = "{0}は{1}文字までです。")]
        public string UserName { get; set; }

        /// <summary>姓</summary>
        [Display(Name = "姓")]
        [Comment("姓")]
        [Required]
        [MaxLength(30, ErrorMessage = "{0}は{1}文字までです。")]
        public string FamilyName { get; set; }

        /// <summary>名</summary>
        [Display(Name = "名")]
        [Comment("名")]
        [Required]
        [MaxLength(30, ErrorMessage = "{0}は{1}文字までです。")]
        public string GivenName { get; set; }

        /// <summary>フルネーム</summary>
        [Display(Name = "フルネーム")]
        [Comment("フルネーム")]
        public string FullName
        {
            get
            {
                return $"{FamilyName} {GivenName}";
            }
        }

        /// <summary>ロール情報</summary>
        public virtual ICollection<RoleEntity> Roles { get; set; }
    }
}