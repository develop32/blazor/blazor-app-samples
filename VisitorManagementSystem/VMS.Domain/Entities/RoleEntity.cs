﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace VMS.Domain.Entities
{
    /// <summary>
    /// ロールエンティティクラス
    /// </summary>
    [Table("Roles")]
    [Comment("ロール")]
    public class RoleEntity : AbstractEntity
    {
        /// <summary>ロール名</summary>
        [Display(Name = "ロール名")]
        [Comment("ロール名")]
        [Required]
        [MaxLength(50, ErrorMessage = "{0}は{1}文字までです。")]
        public string Name { get; set; }

        /// <summary>ユーザ情報</summary>
        public virtual ICollection<UserEntity> Users { get; set; }
    }
}