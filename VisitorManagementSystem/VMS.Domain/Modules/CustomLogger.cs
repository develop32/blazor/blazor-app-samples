﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using VMS.Domain.Entities;
using VMS.Domain.Repositories;
using VMS.Domain.ValueObjects;

namespace VMS.Domain.Modules
{
    /// <summary>
    /// ロガー
    /// </summary>
    public class CustomLogger : ICustomLogger
    {
        private ILogsRepository _logsRepository;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="logsRepository">ログレポジトリ</param>
        public CustomLogger(ILogsRepository logsRepository)
        {
            _logsRepository = logsRepository;
        }

        /// <inheritdoc/>
        public async void Debug(string message, string data = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int? callerLineNumber = null, string stackTrace = "", string sql = "", string remarks = "")
        {
            await AddLog(ErrorEevel.Debug, message, data, callerFilePath, callerMemberName, callerLineNumber, stackTrace, sql, remarks);
        }

        /// <inheritdoc/>
        public async void Info(string message, string data = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int? callerLineNumber = null, string stackTrace = "", string sql = "", string remarks = "")
        {
            await AddLog(ErrorEevel.Info, message, data, callerFilePath, callerMemberName, callerLineNumber, stackTrace, sql, remarks);
        }

        /// <inheritdoc/>
        public async void Warn(string message, string data = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int? callerLineNumber = null, string stackTrace = "", string sql = "", string remarks = "")
        {
            await AddLog(ErrorEevel.Warn, message, data, callerFilePath, callerMemberName, callerLineNumber, stackTrace, sql, remarks);
        }

        /// <inheritdoc/>
        public async void Error(string message, string data = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int? callerLineNumber = null, string stackTrace = "", string sql = "", string remarks = "")
        {
            await AddLog(ErrorEevel.Error, message, data, callerFilePath, callerMemberName, callerLineNumber, stackTrace, sql, remarks);
        }

        /// <inheritdoc/>
        public async void Fatal(string message, string data = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int? callerLineNumber = null, string stackTrace = "", string sql = "", string remarks = "")
        {
            await AddLog(ErrorEevel.Fatal, message, data, callerFilePath, callerMemberName, callerLineNumber, stackTrace, sql, remarks);
        }

        private async Task AddLog(
            ErrorEevel errorEevel, string message, string data = "", string fileName = "", string memberName = "", int? lineNo = null, string stackTrace = "", string sql = "", string remarks = "")
        {
            // カレントディレクトリ
            DirectoryInfo currentDirectory = new(Environment.CurrentDirectory);

            LogEntity log = new()
            {
                ErrorEevel = errorEevel,
                Message = message,
                Data = data,
                FileName = fileName.Replace(currentDirectory.Parent.FullName, string.Empty),
                MemberName = memberName,
                LineNo = lineNo,
                StackTrace = stackTrace,
                Sql = sql,
                Remarks = remarks,
            };

            try
            {
                await _logsRepository.AddLog(log);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}