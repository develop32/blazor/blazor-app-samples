﻿using System.Runtime.CompilerServices;

namespace VMS.Domain.Modules
{
    /// <summary>
    /// カスタムロガーインターフェース
    /// </summary>
    public interface ICustomLogger
    {
        /// <summary>
        /// デバッグログを出力します。
        /// </summary>
        /// <param name="message">メッセージ</param>
        /// <param name="data">補足データ</param>
        /// <param name="callerFilePath">ファイルパス</param>
        /// <param name="callerMemberName">メンバ</param>
        /// <param name="callerLineNumber">行</param>
        /// <param name="stackTrace">スタックトレース</param>
        /// <param name="sql">SQL</param>
        /// <param name="remarks">備考</param>
        public void Debug(
            string message, string data = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int? callerLineNumber = null, string stackTrace = "", string sql = "", string remarks = "");

        /// <summary>
        /// 情報ログを出力します。
        /// </summary>
        /// <param name="message">メッセージ</param>
        /// <param name="data">補足データ</param>
        /// <param name="callerFilePath">ファイルパス</param>
        /// <param name="callerMemberName">メンバ</param>
        /// <param name="callerLineNumber">行</param>
        /// <param name="stackTrace">スタックトレース</param>
        /// <param name="sql">SQL</param>
        /// <param name="remarks">備考</param>
        public void Info(
            string message, string data = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int? callerLineNumber = null, string stackTrace = "", string sql = "", string remarks = "");

        /// <summary>
        /// 警告ログを出力します。
        /// </summary>
        /// <param name="message">メッセージ</param>
        /// <param name="data">補足データ</param>
        /// <param name="callerFilePath">ファイルパス</param>
        /// <param name="callerMemberName">メンバ</param>
        /// <param name="callerLineNumber">行</param>
        /// <param name="stackTrace">スタックトレース</param>
        /// <param name="sql">SQL</param>
        /// <param name="remarks">備考</param>
        public void Warn(
            string message, string data = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int? callerLineNumber = null, string stackTrace = "", string sql = "", string remarks = "");

        /// <summary>
        /// エラーログを出力します。
        /// </summary>
        /// <param name="message">メッセージ</param>
        /// <param name="data">補足データ</param>
        /// <param name="callerFilePath">ファイルパス</param>
        /// <param name="callerMemberName">メンバ</param>
        /// <param name="callerLineNumber">行</param>
        /// <param name="stackTrace">スタックトレース</param>
        /// <param name="sql">SQL</param>
        /// <param name="remarks">備考</param>
        public void Error(
            string message, string data = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int? callerLineNumber = null, string stackTrace = "", string sql = "", string remarks = "");

        /// <summary>
        /// 重大なエラーを出力します。
        /// </summary>
        /// <param name="message">メッセージ</param>
        /// <param name="data">補足データ</param>
        /// <param name="callerFilePath">ファイルパス</param>
        /// <param name="callerMemberName">メンバ</param>
        /// <param name="callerLineNumber">行</param>
        /// <param name="stackTrace">スタックトレース</param>
        /// <param name="sql">SQL</param>
        /// <param name="remarks">備考</param>
        public void Fatal(
            string message, string data = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int? callerLineNumber = null, string stackTrace = "", string sql = "", string remarks = "");
    }
}