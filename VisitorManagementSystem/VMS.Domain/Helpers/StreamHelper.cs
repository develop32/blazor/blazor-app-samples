﻿using System.IO;
using System.Threading.Tasks;

namespace VMS.Domain.Helpers
{
    /// <summary>
    /// ストリームヘルパークラス
    /// </summary>
    public static class StreamHelper
    {
        /// <summary>
        /// バイト配列に変換します。
        /// </summary>
        /// <param name="stream">ストリーム</param>
        /// <returns>バイト配列</returns>
        public static async Task<byte[]> ToBinary(this Stream stream)
        {
            using MemoryStream memoryStream = new();
            await stream.CopyToAsync(memoryStream);
            return memoryStream.ToArray();
        }
    }
}