﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace VMS.Domain.Helpers
{
    /// <summary>
    /// ディスプレイ属性ヘルパークラス
    /// </summary>
    public static class DisplayAttributeHelper
    {
        /// <summary>
        /// ディスプレイ属性の名前を取得します。
        /// </summary>
        /// <typeparam name="TModel">モデルの型</typeparam>
        /// <typeparam name="TProperty">プロパティの型</typeparam>
        /// <param name="model">モデル</param>
        /// <param name="expression">式</param>
        /// <returns>ディスプレイ属性の名前</returns>
#pragma warning disable IDE0060 // 未使用のパラメーターを削除します

        public static string GetDisplayName<TModel, TProperty>(this TModel model, Expression<Func<TModel, TProperty>> expression)
#pragma warning restore IDE0060 // 未使用のパラメーターを削除します
        {
            DisplayAttribute displayAttribute = GetDisplayAttribute(expression);
            return (displayAttribute != null) ? displayAttribute.Name : string.Empty;
        }

        /// <summary>
        /// ディスプレイ属性の説明を取得します。
        /// </summary>
        /// <typeparam name="TModel">モデルの型</typeparam>
        /// <typeparam name="TProperty">プロパティの型</typeparam>
        /// <param name="model">モデル</param>
        /// <param name="expression">式</param>
        /// <returns>ディスプレイ属性の説明</returns>
#pragma warning disable IDE0060 // 未使用のパラメーターを削除します

        public static string GetDisplayDescription<TModel, TProperty>(this TModel model, Expression<Func<TModel, TProperty>> expression)
#pragma warning restore IDE0060 // 未使用のパラメーターを削除します
        {
            DisplayAttribute displayAttribute = GetDisplayAttribute(expression);
            return (displayAttribute != null) ? displayAttribute.Description : string.Empty;
        }

        private static DisplayAttribute GetDisplayAttribute<TModel, TProperty>(Expression<Func<TModel, TProperty>> expression)
        {
            Type type = typeof(TModel);
            MemberExpression memberExpression = (MemberExpression)expression.Body;
            string propertyName = (memberExpression.Member is PropertyInfo) ? memberExpression.Member.Name : null;

            // First look into attributes on a type and it's parents
            DisplayAttribute attr;
            attr = (DisplayAttribute)type.GetProperty(propertyName).GetCustomAttributes(typeof(DisplayAttribute), true).SingleOrDefault();

            // Look for [MetadataType] attribute in type hierarchy
            // http://stackoverflow.com/questions/1910532/attribute-isdefined-doesnt-see-attributes-applied-with-metadatatype-class
            if (attr == null)
            {
                MetadataTypeAttribute metadataType = (MetadataTypeAttribute)type.GetCustomAttributes(typeof(MetadataTypeAttribute), true).FirstOrDefault();
                if (metadataType != null)
                {
                    var property = metadataType.MetadataClassType.GetProperty(propertyName);
                    if (property != null)
                    {
                        attr = (DisplayAttribute)property.GetCustomAttributes(typeof(DisplayAttribute), true).SingleOrDefault();
                    }
                }
            }

            return attr;
        }
    }
}