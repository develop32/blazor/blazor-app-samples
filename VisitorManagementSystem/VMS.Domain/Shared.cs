﻿using VMS.Domain.Entities;

namespace VMS.Domain
{
    /// <summary>
    /// シェアクラス
    /// </summary>
    public class Shared
    {
        /// <summary>ログインユーザ</summary>
        public static UserEntity LoginUser { get; set; }

        /// <summary>ダウンロードフォルダ</summary>
        public static string DownloadFolder { get; set; }
    }
}