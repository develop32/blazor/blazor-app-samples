﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace VMS.Infrastructure.Oracle
{
    /// <summary>
    /// 基底Oracleアクセスクラス
    /// </summary>
    public abstract class AbstractOracle
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        protected AbstractOracle(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }

        /// <summary>
        /// DBコンテキスト
        /// </summary>
        public ApplicationDbContext DbContext { get; private set; }

        /// <summary>
        /// 変更を追跡しているエンティティの状態をコンソールに表示します。
        /// </summary>
        protected void DisplayStates()
        {
            IEnumerable<EntityEntry> entries = DbContext.ChangeTracker.Entries();

            foreach (var entry in entries)
            {
                Debug.WriteLine($"Entity: {entry.Entity.GetType().Name}, State: {entry.State}");
            }
        }
    }
}