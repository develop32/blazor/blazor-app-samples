﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VMS.Domain.Entities;
using VMS.Domain.Repositories;

namespace VMS.Infrastructure.Oracle
{
    /// <summary>
    /// ユーザテーブルアクセスクラス
    /// </summary>
    public class UsersOracle : AbstractOracle, IUsersRepository
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        public UsersOracle(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<UserEntity>> GetAllUsers()
        {
            return await DbContext.Users
                .AsNoTracking()
                .ToListAsync();
        }
    }
}