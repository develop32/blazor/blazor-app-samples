﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VMS.Domain.Entities;
using VMS.Domain.Repositories;

namespace VMS.Infrastructure.Oracle
{
    /// <summary>
    /// ログテーブルアクセスクラス
    /// </summary>
    public class LogsOracle : AbstractOracle, ILogsRepository
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        public LogsOracle(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }

        /// <inheritdoc/>
        public async Task AddLog(LogEntity log)
        {
                await DbContext.Logs.AddAsync(log);
                await DbContext.SaveChangesAsync();
                DbContext.Entry(log).State = EntityState.Detached;
        }
    }
}