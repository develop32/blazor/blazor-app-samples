﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VMS.Domain;
using VMS.Domain.Entities;

namespace VMS.Infrastructure.Oracle
{
    /// <summary>
    /// DBコンテキスト
    /// </summary>
    public class ApplicationDbContext : DbContext
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="options">オプション</param>
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }

        /// <summary>ユーザテーブル</summary>
        public DbSet<UserEntity> Users { get; set; }

        /// <summary>ロールテーブル</summary>
        public DbSet<RoleEntity> Roles { get; set; }

        /// <summary>来客申請テーブル</summary>
        public DbSet<ApplicationEntity> Applications { get; set; }

        /// <summary>スケジュールテーブル</summary>
        public DbSet<ScheduleEntity> Schedules { get; set; }

        /// <summary>来客者テーブル</summary>
        public DbSet<VisitorEntity> Visitors { get; set; }

        /// <summary>送信メールテーブル</summary>
        public DbSet<SendingMailEntity> SendingMails { get; set; }

        /// <summary>メール送信先テーブル</summary>
        public DbSet<MailDestinationEntity> MailDestinations { get; set; }

        /// <summary>ログテーブル</summary>
        public DbSet<LogEntity> Logs { get; set; }

        /// <inheritdoc/>
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
        {
            foreach (var entry in ChangeTracker.Entries<IAbstractEntity>().ToList())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedOn = DateTime.Now;
                        entry.Entity.CreatedBy = Shared.LoginUser == null
                            ? 0
                            : Shared.LoginUser.Id;
                        break;

                    case EntityState.Modified:
                        entry.Entity.UpdatedOn = DateTime.Now;
                        entry.Entity.UpdatedBy = Shared.LoginUser == null
                            ? 0
                            : Shared.LoginUser.Id;
                        break;
                }
            }

            return await base.SaveChangesAsync(cancellationToken);
        }

        /// <summary>
        /// 設定処理
        /// </summary>
        /// <param name="optionsBuilder">オプションビルダー</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }

        /// <summary>
        /// モデル生成時処理
        /// </summary>
        /// <param name="modelBuilder">モデルビルダー</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RoleEntity>()
                .HasData(
                    new RoleEntity { Id = -1, Name = "Administrator" },
                    new RoleEntity { Id = -2, Name = "Guest" });

            modelBuilder.Entity<UserEntity>()
                .HasData(
                    new UserEntity { Id = -1, UserName = "Ichiro", FamilyName = "山田", GivenName = "一郎" },
                    new UserEntity { Id = -2, UserName = "Jiro", FamilyName = "山田", GivenName = "二郎" },
                    new UserEntity { Id = -3, UserName = "Saburo", FamilyName = "山田", GivenName = "三郎" });

            modelBuilder.Entity<ApplicationEntity>(entity =>
            {
                entity.HasData(
                    new ApplicationEntity
                    {
                        Id = -1,
                        Purpose = "商談",
                        FromDatetime = new DateTime(2022, 1, 1, 9, 0, 0),
                        ToDatetime = new DateTime(2022, 1, 1, 17, 0, 0),
                    });
            });

            modelBuilder.Entity<VisitorEntity>()
                .HasData(
                    new VisitorEntity { Id = -1, ApplicationId = -1, CompanyName = "A社", Name = "Aさん" },
                    new VisitorEntity { Id = -2, ApplicationId = -1, CompanyName = "B社", Name = "Bさん" },
                    new VisitorEntity { Id = -3, ApplicationId = -1, CompanyName = "C社", Name = "Cさん" });

            modelBuilder.Entity<ScheduleEntity>()
                .HasData(
                    new ScheduleEntity
                    {
                        Id = -1,
                        ApplicationId = -1,
                        FromDatetime = new DateTime(2022, 1, 1, 9, 0, 0),
                        ToDatetime = new DateTime(2022, 1, 1, 9, 30, 0),
                        Overview = "お出迎え",
                        Details = "ウェルカムボードでお出迎え",
                    },
                    new ScheduleEntity
                    {
                        Id = -2,
                        ApplicationId = -1,
                        FromDatetime = new DateTime(2022, 1, 1, 9, 30, 0),
                        ToDatetime = new DateTime(2022, 1, 1, 10, 00, 0),
                        Overview = "本日の予定説明",
                        Details = "パワポでスケジュール表示",
                    });

            modelBuilder.Entity<UserEntity>()
                .HasMany(p => p.Roles)
                .WithMany(t => t.Users)
                .UsingEntity<Dictionary<string, object>>(
                    "UserRoles",
                    x => x.HasOne<RoleEntity>().WithMany(),
                    x => x.HasOne<UserEntity>().WithMany());
        }
    }
}