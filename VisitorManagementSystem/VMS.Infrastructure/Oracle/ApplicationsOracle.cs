﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VMS.Domain.Dtos;
using VMS.Domain.Entities;
using VMS.Domain.Repositories;
using VMS.Domain.ValueObjects;

namespace VMS.Infrastructure.Oracle
{
    /// <summary>
    /// 来客申請テーブルアクセスクラス
    /// </summary>
    public class ApplicationsOracle : AbstractOracle, IApplicationsRepository
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        public ApplicationsOracle(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }

        /// <inheritdoc/>
        public async Task<ApplicationDetailDto> CreateApplication(ApplicationDetailButton button, ApplicationDetailDto application)
        {
            ApplicationEntity dbApplication = new()
            {
                Schedules = new List<ScheduleEntity>(),
                Visitors = new List<VisitorEntity>(),
                AttachedFiles = new List<AttachedFileEntity>(),
            };

            // -----------------------------------------------
            // 来客申請情報
            // -----------------------------------------------
            SetApplication(button, dbApplication, application);

            // -----------------------------------------------
            // スケジュール情報
            // -----------------------------------------------
            SetSchedules(dbApplication, application);

            // -----------------------------------------------
            // 来客者情報
            // -----------------------------------------------
            SetVisitors(dbApplication, application);

            // -----------------------------------------------
            // 添付ファイル情報
            // -----------------------------------------------
            SetAttachedFiles(dbApplication, application);

            // DBへ追加
            var addedApplication = DbContext.Applications.Add(dbApplication);
            await DbContext.SaveChangesAsync();
            DbContext.Entry(dbApplication).State = EntityState.Detached;

            return new ApplicationDetailDto(addedApplication.Entity);
        }

        /// <inheritdoc/>
        public async Task<ApplicationDetailDto> GetApplication(int id)
        {
            ApplicationEntity dbApplication = await DbContext.Applications
                .FirstOrDefaultAsync(a => a.Id == id);

            ApplicationDetailDto application = new(dbApplication);

            DbContext.Entry(dbApplication).State = EntityState.Detached;
            return application;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<ApplicationEntity>> GetApplications(ApplicationListSearchItemDto searchItem)
        {
            return await DbContext.Applications
                .AsNoTracking()
                .Where(a => a.Purpose.Contains(searchItem.Purpose))
                .ToListAsync();
        }

        /// <inheritdoc/>
        public async Task<ApplicationDetailDto> UpdateApplication(ApplicationDetailButton button, int id, ApplicationDetailDto application)
        {
            ApplicationEntity dbApplication = await DbContext.Applications
                .FirstOrDefaultAsync(a => a.Id == id);

            // -----------------------------------------------
            // 来客申請情報
            // -----------------------------------------------
            SetApplication(button, dbApplication, application);

            // -----------------------------------------------
            // スケジュール情報
            // -----------------------------------------------
            SetSchedules(dbApplication, application);

            // -----------------------------------------------
            // 来客者情報
            // -----------------------------------------------
            SetVisitors(dbApplication, application);

            // -----------------------------------------------
            // 添付ファイル情報
            // -----------------------------------------------
            SetAttachedFiles(dbApplication, application);

            DisplayStates();

            await DbContext.SaveChangesAsync();

            DbContext.Entry(dbApplication).State = EntityState.Detached;

            return await GetApplication(id);
        }

        /// <inheritdoc/>
        public async Task DeleteApplication(int id)
        {
            ApplicationEntity dbApplication = await DbContext.Applications.FindAsync(id);
            if (dbApplication == null)
            {
                return;
            }

            DbContext.Applications.Remove(dbApplication);
            DisplayStates();
            await DbContext.SaveChangesAsync();
            DisplayStates();
        }

        /// <summary>
        /// 来客申請情報を設定します。
        /// </summary>
        /// <param name="dbApplication">DBの来客申請</param>
        /// <param name="dtoApplication">画面の来客申請</param>
        private static void SetApplication(ApplicationDetailButton button, ApplicationEntity dbApplication, ApplicationDetailDto dtoApplication)
        {
            // 目的
            dbApplication.Purpose = dtoApplication.Purpose;

            // 開始日付
            dbApplication.FromDatetime = dtoApplication.FromDatetime + dtoApplication.FromTime;

            // 終了日付
            dbApplication.ToDatetime = dtoApplication.ToDatetime + dtoApplication.ToTime;

            // ステータス
            if (button == ApplicationDetailButton.SaveTemporarily)
            {
                dbApplication.ApplicationStatus = ApplicationStatus.TemporarySaving;
            }
            else if (button == ApplicationDetailButton.Submit)
            {
                dbApplication.ApplicationStatus = ApplicationStatus.WaitingForApproval1;
            }
            else
            {
                throw new ArgumentOutOfRangeException(nameof(button), $"ボタンが不正です。({button.Value})");
            }
        }

        /// <summary>
        /// スケジュール情報を設定します。
        /// </summary>
        /// <param name="dbApplication">DBの来客申請</param>
        /// <param name="dtoApplication">画面の来客申請</param>
        private static void SetSchedules(ApplicationEntity dbApplication, ApplicationDetailDto dtoApplication)
        {
            // 削除
            foreach (ScheduleEntity dbSchedule in dbApplication.Schedules.ToList())
            {
                // DBにはあるが画面にないスケジュールの場合
                if (!dtoApplication.Schedules.Any(s => s.Id == dbSchedule.Id))
                {
                    dbApplication.Schedules.Remove(dbSchedule);
                }
            }

            // 更新 Or 追加
            foreach (ScheduleEntity dtoSchedule in dtoApplication.Schedules.ToList())
            {
                if (dtoSchedule.IsBlank)
                {
                    continue;
                }

                ScheduleEntity dbSchedule = dbApplication.Schedules
                    .FirstOrDefault(s => s.Id == dtoSchedule.Id && s.Id != default);

                if (dbSchedule != null)
                {
                    dbSchedule = dtoSchedule;
                }
                else
                {
                    dbApplication.Schedules.Add(dtoSchedule);
                }
            }
        }

        /// <summary>
        /// 来客者情報を設定します。
        /// </summary>
        /// <param name="dbApplication">DBの来客申請</param>
        /// <param name="dtoApplication">画面の来客申請</param>
        private static void SetVisitors(ApplicationEntity dbApplication, ApplicationDetailDto dtoApplication)
        {
            // 削除
            foreach (VisitorEntity dbVisitor in dbApplication.Visitors.ToList())
            {
                // DBにはあるが画面にない来客者の場合
                if (!dtoApplication.Visitors.Any(s => s.Id == dbVisitor.Id))
                {
                    dbApplication.Visitors.Remove(dbVisitor);
                }
            }

            // 更新 Or 追加
            foreach (VisitorEntity dtoVisitor in dtoApplication.Visitors.ToList())
            {
                if (dtoVisitor.IsBlank)
                {
                    continue;
                }

                VisitorEntity dbSchedule = dbApplication.Visitors
                    .FirstOrDefault(s => s.Id == dtoVisitor.Id && s.Id != default);

                if (dbSchedule != null)
                {
                    dbSchedule = dtoVisitor;
                }
                else
                {
                    dbApplication.Visitors.Add(dtoVisitor);
                }
            }
        }

        /// <summary>
        /// 添付ファイル情報を設定します。
        /// </summary>
        /// <param name="dbApplication">DBの来客申請</param>
        /// <param name="dtoApplication">画面の来客申請</param>
        private static void SetAttachedFiles(ApplicationEntity dbApplication, ApplicationDetailDto dtoApplication)
        {
            // 削除
            foreach (AttachedFileEntity dbAttachedFile in dbApplication.AttachedFiles.ToList())
            {
                if (!dtoApplication.AttachedFiles.Any(s => s.Id == dbAttachedFile.Id))
                {
                    dbApplication.AttachedFiles.Remove(dbAttachedFile);
                }
            }

            // 更新 Or 追加
            foreach (AttachedFileEntity dtodbAttachedFile in dtoApplication.AttachedFiles.ToList())
            {
                AttachedFileEntity dbSchedule = dbApplication.AttachedFiles
                    .FirstOrDefault(s => s.Id == dtodbAttachedFile.Id && s.Id != default);

                if (dbSchedule != null)
                {
                    dbSchedule = dtodbAttachedFile;
                }
                else
                {
                    dbApplication.AttachedFiles.Add(dtodbAttachedFile);
                }
            }
        }
    }
}