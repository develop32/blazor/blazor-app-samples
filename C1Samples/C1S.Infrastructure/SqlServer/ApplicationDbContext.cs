﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using C1S.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace C1S.Infrastructure.SqlServer
{
    /// <summary>
    /// アプリケーションDBコンテキスト
    /// </summary>
    public class ApplicationDbContext : DbContext
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="options">DBコンテキストオプション</param>
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }

        /// <summary>Todo(Todos)テーブル</summary>
        public virtual DbSet<TodoEntity> Todos { get; set; }

        /// <summary>
        /// 変更を保存（コミット）する。
        /// </summary>
        /// <param name="cancellationToken">キャンセルトークン</param>
        /// <returns>更新件数</returns>
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            foreach (var entry in ChangeTracker.Entries<IAbstractEntity>().ToList())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedOn = DateTime.Now;
                        entry.Entity.CreatedBy = "Unknown";
                        break;

                    case EntityState.Modified:
                        entry.Entity.UpdatedOn = DateTime.Now;
                        entry.Entity.UpdatedBy = "Unknown";
                        break;
                }
            }

            return await base.SaveChangesAsync(cancellationToken);
        }

        /// <summary>
        /// モデル作成時の処理
        /// </summary>
        /// <param name="modelBuilder">モデルビルダー</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TodoEntity>(entity =>
            {
                entity.HasData(
                    new TodoEntity { Id = -1, Title = "タイトル1", IsDone = true, CreatedBy = "Unknown", CreatedOn = DateTime.Now },
                    new TodoEntity { Id = -2, Title = "タイトル2", IsDone = false, CreatedBy = "Unknown", CreatedOn = DateTime.Now },
                    new TodoEntity { Id = -3, Title = "タイトル3", IsDone = false, CreatedBy = "Unknown", CreatedOn = DateTime.Now });
            });
        }
    }
}