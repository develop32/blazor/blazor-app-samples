﻿using System.Collections.Generic;
using System.Threading.Tasks;
using C1S.Domain.Entities;
using C1S.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace C1S.Infrastructure.SqlServer
{
    /// <summary>
    /// Todoレポジトリクラス
    /// </summary>
    public class TodosSqlServer : ITodosRepository
    {
        private readonly ApplicationDbContext _dbContext;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        public TodosSqlServer(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<TodoEntity>> GetAllTodos()
        {
            return await _dbContext.Todos.ToListAsync();
        }

        /// <inheritdoc/>
        public async Task<TodoEntity> GetTodo(int id)
        {
            return await _dbContext.Todos.FindAsync(id);
        }
    }
}