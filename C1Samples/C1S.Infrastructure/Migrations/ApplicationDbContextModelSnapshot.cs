﻿// <auto-generated />
using System;
using C1S.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace C1S.Infrastructure.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.13")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("C1S.Domain.Entities.TodoEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasComment("ID:自動採番。")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)")
                        .HasComment("作成者");

                    b.Property<DateTime?>("CreatedOn")
                        .IsRequired()
                        .HasColumnType("DATE")
                        .HasComment("作成日時");

                    b.Property<bool>("IsDone")
                        .HasColumnType("bit")
                        .HasComment("完了");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)")
                        .HasComment("タイトル");

                    b.Property<string>("UpdatedBy")
                        .HasColumnType("nvarchar(max)")
                        .HasComment("更新者");

                    b.Property<DateTime?>("UpdatedOn")
                        .HasColumnType("DATE")
                        .HasComment("更新日時");

                    b.HasKey("Id");

                    b.ToTable("Todos");

                    b
                        .HasComment("Todo");

                    b.HasData(
                        new
                        {
                            Id = -1,
                            CreatedBy = "Unknown",
                            CreatedOn = new DateTime(2021, 12, 26, 10, 46, 37, 760, DateTimeKind.Local).AddTicks(8111),
                            IsDone = true,
                            Title = "タイトル1"
                        },
                        new
                        {
                            Id = -2,
                            CreatedBy = "Unknown",
                            CreatedOn = new DateTime(2021, 12, 26, 10, 46, 37, 761, DateTimeKind.Local).AddTicks(9784),
                            IsDone = false,
                            Title = "タイトル2"
                        },
                        new
                        {
                            Id = -3,
                            CreatedBy = "Unknown",
                            CreatedOn = new DateTime(2021, 12, 26, 10, 46, 37, 761, DateTimeKind.Local).AddTicks(9808),
                            IsDone = false,
                            Title = "タイトル3"
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
