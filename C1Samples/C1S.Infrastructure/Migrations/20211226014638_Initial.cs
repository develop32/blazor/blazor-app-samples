﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace C1S.Infrastructure.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Todos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false, comment: "ID:自動採番。")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "タイトル"),
                    IsDone = table.Column<bool>(type: "bit", nullable: false, comment: "完了"),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: false, comment: "作成者"),
                    CreatedOn = table.Column<DateTime>(type: "DATE", nullable: false, comment: "作成日時"),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "更新者"),
                    UpdatedOn = table.Column<DateTime>(type: "DATE", nullable: true, comment: "更新日時")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Todos", x => x.Id);
                },
                comment: "Todo");

            migrationBuilder.InsertData(
                table: "Todos",
                columns: new[] { "Id", "CreatedBy", "CreatedOn", "IsDone", "Title", "UpdatedBy", "UpdatedOn" },
                values: new object[] { -1, "Unknown", new DateTime(2021, 12, 26, 10, 46, 37, 760, DateTimeKind.Local).AddTicks(8111), true, "タイトル1", null, null });

            migrationBuilder.InsertData(
                table: "Todos",
                columns: new[] { "Id", "CreatedBy", "CreatedOn", "IsDone", "Title", "UpdatedBy", "UpdatedOn" },
                values: new object[] { -2, "Unknown", new DateTime(2021, 12, 26, 10, 46, 37, 761, DateTimeKind.Local).AddTicks(9784), false, "タイトル2", null, null });

            migrationBuilder.InsertData(
                table: "Todos",
                columns: new[] { "Id", "CreatedBy", "CreatedOn", "IsDone", "Title", "UpdatedBy", "UpdatedOn" },
                values: new object[] { -3, "Unknown", new DateTime(2021, 12, 26, 10, 46, 37, 761, DateTimeKind.Local).AddTicks(9808), false, "タイトル3", null, null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Todos");
        }
    }
}
