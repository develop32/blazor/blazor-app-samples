﻿using System.Collections.Generic;
using System.Threading.Tasks;
using C1S.Domain.Entities;

namespace C1S.Domain.Repositories
{
    /// <summary>
    /// Todoレポジトリインターフェース
    /// </summary>
    public interface ITodosRepository
    {
        /// <summary>
        /// すべてのTodoを取得します。
        /// </summary>
        /// <returns>すべてのTodo</returns>
        public Task<IEnumerable<TodoEntity>> GetAllTodos();

        /// <summary>
        /// Todoを取得します。
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns>Todo</returns>
        public Task<TodoEntity> GetTodo(int id);
    }
}