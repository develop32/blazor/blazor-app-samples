﻿using System;

namespace C1S.Domain.Entities
{
    /// <summary>
    /// 基底エンティティインターフェース
    /// </summary>
    public interface IAbstractEntity
    {
        /// <summary>ID</summary>
        public int Id { get; set; }

        /// <summary>作成者</summary>
        public string CreatedBy { get; set; }

        /// <summary>作成日時</summary>
        public DateTime? CreatedOn { get; set; }

        /// <summary>更新者</summary>
        public string UpdatedBy { get; set; }

        /// <summary>更新日時</summary>
        public DateTime? UpdatedOn { get; set; }
    }
}