﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace C1S.Domain.Entities
{
    /// <summary>
    /// Todoエンティティクラス
    /// </summary>
    [Table("Todos")]
    [Comment("Todo")]
    public class TodoEntity : AbstractEntity
    {
        /// <summary>タイトル</summary>
        [Display(Name = "タイトル")]
        [Comment("タイトル")]
        [Required(ErrorMessage = "{0}は入力必須です。")]
        [StringLength(100, ErrorMessage = "{0}は{1}以内で入力してください。")]
        public string Title { get; set; }

        /// <summary>完了</summary>
        [Display(Name = "完了")]
        [Comment("完了")]
        [Required]
        public bool IsDone { get; set; }
    }
}