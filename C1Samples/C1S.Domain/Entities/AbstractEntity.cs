﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace C1S.Domain.Entities
{
    /// <summary>
    /// 基底エンティティクラス
    /// </summary>
    public class AbstractEntity : IAbstractEntity
    {
        /// <summary>ID</summary>
        [Display(Name = "ID")]
        [Comment("ID:自動採番。")]
        [Key]
        public int Id { get; set; }

        /// <summary>作成者</summary>
        [Display(Name = "作成者")]
        [Comment("作成者")]
        [Required]
        [DefaultValue("Unknown")]
        public string CreatedBy { get; set; }

        /// <summary>作成日時</summary>
        [Display(Name = "作成日時")]
        [Comment("作成日時")]
        [Required]
        [Column(TypeName = "DATE")]
        [DefaultValue("GETDATE()")]
        public DateTime? CreatedOn { get; set; }

        /// <summary>更新者</summary>
        [Display(Name = "更新者")]
        [Comment("更新者")]
        public string UpdatedBy { get; set; }

        /// <summary>更新日時</summary>
        [Display(Name = "更新日時")]
        [Comment("更新日時")]
        [Column(TypeName = "DATE")]
        public DateTime? UpdatedOn { get; set; }
    }
}