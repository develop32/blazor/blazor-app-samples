﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SA.Server.Data;
using SA.Shared.Entities;

namespace SA.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TagsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public TagsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Tags
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TagEntity>>> GetTags()
        {
            return await _context.Tags.ToListAsync();
        }

        //GET: api/Tags/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TagEntity>> GetTagEntity(int id)
        {
            var tagEntity = await _context.Tags.FindAsync(id);

            if (tagEntity == null)
            {
                return NotFound();
            }

            return tagEntity;
        }

        // GET: api/Tags/xxx
        //[HttpGet("{tagName}")]
        // public async Task<ActionResult<TagEntity>> GetTagByName(string tagName)
        // {
        //     var tag = await _context.Tags.Where(t => t.TagName.Equals(tagName)).FirstOrDefaultAsync();
        //     return tag;
        // }

        // PUT: api/Tags/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTagEntity(int id, TagEntity tagEntity)
        {
            if (id != tagEntity.Id)
            {
                return BadRequest();
            }

            _context.Entry(tagEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TagEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Tags
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<TagEntity>> PostTagEntity(TagEntity tagEntity)
        {
            _context.Tags.Add(tagEntity);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTagEntity", new { id = tagEntity.Id }, tagEntity);
        }

        // DELETE: api/Tags/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTagEntity(int id)
        {
            var tagEntity = await _context.Tags.FindAsync(id);
            if (tagEntity == null)
            {
                return NotFound();
            }

            _context.Tags.Remove(tagEntity);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool TagEntityExists(int id)
        {
            return _context.Tags.Any(e => e.Id == id);
        }
    }
}