﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SA.Shared.Entities;

namespace SA.Server.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }

        public virtual DbSet<ProgramEntity> Programs { get; set; }
        public virtual DbSet<TagEntity> Tags { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProgramEntity>()
                .HasMany(p => p.Tags)
                .WithMany(t => t.Programs)
                .UsingEntity<Dictionary<string, object>>(
                    "ProgramTags",
                    x => x.HasOne<TagEntity>().WithMany(),
                    x => x.HasOne<ProgramEntity>().WithMany());
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
        {
            foreach (var entry in ChangeTracker.Entries<IAbstractEntity>().ToList())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedOn = DateTime.Now;
                        entry.Entity.CreatedBy = "Unknown";
                        entry.Entity.UpdatedOn = DateTime.Now;
                        entry.Entity.UpdatedBy = "Unknown";
                        break;

                    case EntityState.Modified:
                        entry.Entity.UpdatedOn = DateTime.Now;
                        entry.Entity.UpdatedBy = "Unknown";
                        break;
                }
            }

            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}