﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SA.Shared.Entities
{
    [Table("Tags")]
    public class TagEntity : AbstractEntity
    {
        /// <summary>タグ名</summary>
        [Display(Name = "タグ名")]
        [Required]
        [MaxLength(50, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string TagName { get; set; }

        /// <summary>プログラム情報</summary>
        [Display(Name = "プログラム情報")]
        public virtual ICollection<ProgramEntity> Programs { get; set; }
    }
}