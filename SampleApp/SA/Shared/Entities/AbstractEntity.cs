﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SA.Shared.Entities
{
    public abstract class AbstractEntity : IAbstractEntity
    {
        /// <summary>ID</summary>
        [Display(Name = "ID")]
        public int Id { get; set; }

        /// <inheritdoc/>
        [Display(Name = "作成者")]
        [Required]
        [MaxLength(50)]
        public string CreatedBy { get; set; } = "Unknown";

        /// <inheritdoc/>
        [Display(Name = "作成日時")]
        [Required]
        public DateTime CreatedOn { get; set; } = DateTime.Now;

        /// <inheritdoc/>
        [Display(Name = "更新者")]
        [Required]
        [MaxLength(50)]
        public string UpdatedBy { get; set; } = "Unknown";

        /// <inheritdoc/>
        [Display(Name = "更新日時")]
        [Required]
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
    }
}