﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SA.Shared.Entities
{
    [Table("Programs")]
    public class ProgramEntity : AbstractEntity
    {
        /// <summary>プログラムID</summary>
        [Display(Name = "プログラムID")]
        [Required]
        [MaxLength(50, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string ProgramId { get; set; }

        /// <summary>プログラム名</summary>
        [Display(Name = "プログラム名")]
        [Required]
        [MaxLength(50, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string ProgramName { get; set; }

        /// <summary>目的</summary>
        [Display(Name = "目的")]
        [MaxLength(500, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string Purpose { get; set; }

        /// <summary>機能概要</summary>
        [Display(Name = "機能概要")]
        [MaxLength(500, ErrorMessage = "{0}は{1}文字以内で入力してください。")]
        public string FunctionalOverview { get; set; }

        /// <summary>タグ情報</summary>
        [Display(Name = "タグ情報")]
        public virtual ICollection<TagEntity> Tags { get; set; }
    }
}