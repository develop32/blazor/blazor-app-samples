using BWO.Server.Data;
using BWO.Server.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BWO.Server
{
    /// <summary>
    /// スタートアップクラス。<br />
    /// アプリケーションの動作を定義するクラスになります。
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// コンストラクタ。<br />
        /// スタートアップクラス（<see cref="Startup"/>）の新しいインスタンスを初期化します。
        /// </summary>
        /// <param name="configuration">構成</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// キー／バリュー形式のアプリケーション構成プロパティのセットを表します。
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// このメソッドはランタイムによって呼び出されます。<br />
        /// このメソッドを使用して、コンテナにサービスを追加します。<br />
        /// アプリケーションを構成する方法の詳細については、<br />
        /// <see cref="https://go.microsoft.com/fwlink/?LinkID=398940"/>を参照ください。
        /// </summary>
        /// <param name="services">サービス</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddIdentityServer()
                .AddApiAuthorization<ApplicationUser, ApplicationDbContext>();

            services.AddAuthentication()
                .AddIdentityServerJwt();

            services.AddControllersWithViews();
            services.AddRazorPages();
        }

        /// <summary>
        /// このメソッドはランタイムによって呼び出されます。<br />
        /// このメソッドを使用してHTTPリクエストパイプラインを設定します。
        /// </summary>
        /// <param name="app">アプリケーションビルダー</param>
        /// <param name="env">環境</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
                app.UseWebAssemblyDebugging();
            }
            else
            {
                app.UseExceptionHandler("/Error");

                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseIdentityServer();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
                endpoints.MapFallbackToFile("index.html");
            });
        }
    }
}