﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace BWO.Server
{
    /// <summary>
    /// プログラムクラス。
    /// エントリーポイント。
    /// </summary>
    public class Program
    {
        /// <summary>
        /// メイン処理。
        /// プログラム実行時最初に実行されます。
        /// </summary>
        /// <param name="args">引数</param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// ホストビルダーを作成します。
        /// </summary>
        /// <param name="args">引数</param>
        /// <returns>ホストビルダー</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}