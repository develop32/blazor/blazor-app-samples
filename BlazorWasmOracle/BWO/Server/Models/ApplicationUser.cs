﻿using Microsoft.AspNetCore.Identity;

namespace BWO.Server.Models
{
    /// <summary>
    /// アプリケーションユーザクラス。
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
    }
}