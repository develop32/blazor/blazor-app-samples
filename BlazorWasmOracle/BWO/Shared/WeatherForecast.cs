﻿using System;

namespace BWO.Shared
{
    /// <summary>
    /// 天気予測エンティティクラス。
    /// </summary>
    public class WeatherForecast
    {
        /// <summary>
        /// 日付を取得または設定します。
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// 温度(℃)を取得または設定します。
        /// </summary>
        public int TemperatureC { get; set; }

        /// <summary>
        /// サマリを取得または設定します。
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// 温度(°F)を取得します。
        /// </summary>
        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
    }
}